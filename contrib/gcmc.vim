" Gcmc syntax file
" Heavily copied from C syntax and adapted.
"
" To add automatic syntax highlighting to the file extension you need to add
" it to the ~/.vimrc file:
" augroup filetypedetect
"   au BufRead,BufNewFile *.gcmc          setf gcmc
" augroup END
"
" Then copy (or link) the gcmc.vim file to  ~/.vim/syntax/gcmc.vim
" After that you should have syntax highlighting on your scripts.


" Quit when a (custom) syntax file was already loaded
if exists("b:current_syntax")
  finish
endif

syn sync fromstart

" A bunch of useful gcmc keywords
syn keyword	gcmcStatement	goto break return continue function local include const
syn keyword	gcmcConditional	if elif else
syn keyword	gcmcRepeat	while for do foreach repeat
syn keyword	gcmcTodo	contained TODO FIXME XXX
syn keyword	gcmcType	string scalar vector vectorlist void

syn keyword	gcmcBuiltin	goto goto_r move move_r
syn keyword	gcmcBuiltin	arc_cw arc_cw_r arc_ccw arc_ccw_r
syn keyword	gcmcBuiltin	coolant drill dwell feedmode feedrate plane pathmode spindle spindlespeed toolchange
syn keyword	gcmcBuiltin	abs acos asin atan atan_xy atan_xz atan_yz ceil cos exp floor log10 log2 loge pi pow round sin sqrt tan sign
syn keyword	gcmcBuiltin	error literal message warning comment undef
syn keyword	gcmcBuiltin	isfloat isint isscalar isstring isundef isvectorlist isvector
syn keyword	gcmcBuiltin	isangle isdistance isdeg israd ismm isinch isnone isdefined isconst
syn keyword	gcmcBuiltin	issvg isdxf isgcode ismodemm isrelative
syn keyword	gcmcBuiltin	layer layerstack typeset relocate
syn keyword	gcmcBuiltin	count length normalize position head tail delete insert
syn keyword	gcmcBuiltin	rotate_xy rotate_xz rotate_yz scale reverse
syn keyword	gcmcBuiltin	deg2rad rad2deg to_in to_inch to_mm to_none to_deg to_rad to_native to_float to_int
syn keyword	gcmcBuiltin	to_chr to_val to_string to_distance
syn keyword	gcmcBuiltin	fixpos_set fixpos_restore fixpos_store pause
syn keyword	gcmcBuiltin	circle_cw circle_ccw circle_cw_r circle_ccw_r
syn keyword	gcmcBuiltin	svg_closepath linewidth lineopacity linecolor

syn keyword	gcmcConst	GCMC_VERSION_STR GCMC_VERSION GCMC_VERSION_MAJOR GCMC_VERSION_MINOR GCMC_VERSION_POINT
syn keyword	gcmcConst	PLANE_QUERY PLANE_XY PLANE_XZ PLANE_YZ
syn keyword	gcmcConst	COOLANT_OFF COOLANT_MIST COOLANT_FLOOD COOLANT_ALL COOLANT_MISTFLOOD
syn keyword	gcmcConst	FEEDMODE_INVERSE FEEDMODE_UPM FEEDMODE_UPR
syn keyword	gcmcConst	FONTF_BOLD FONTF_ITALIC
syn keyword	gcmcConst	FONT_HSANS_1 FONT_HSANS_2 FONT_HSCRIPT_1 FONT_HSCRIPT_2 FONT_HTIMES
syn keyword	gcmcConst	FONT_HSANS_1_RS FONT_HTIMES_BOLD FONT_HTIMES_ITALIC FONT_HTIMES_ITALIC_BOLD

syn match	gcmcSpecial	display contained "\\\(x\x{2}\|\o\{1,3}\|.\|$\)"
syn region	gcmcString	start=+L\="+ skip=+\\\\\|\\"+ end=+"+ contains=gcmcSpecial,@Spell extend

" It's easy to accidentally add a space after a backslash that was intended
" for line continuation.  Some compilers allow it, which makes it
" unpredictable and should be avoided.
syn match	gcmcBadContinuation contained "\\\s\+$"

" cCommentGroup allows adding matches for special things in comments
syn cluster	gcmcCommentGroup	contains=gcmcTodo,gcmcBadContinuation

syn match	gcmcCommentSkip	contained "^\s*\*\($\|\s\+\)"
syn region	gcmcCommentString	contained start=+L\=\\\@<!"+ skip=+\\\\\|\\"+ end=+"+ end=+\*/+me=s-1 contains=gcmcSpecial,gcmcCommentSkip
syn region	gcmcComment2String	contained start=+L\=\\\@<!"+ skip=+\\\\\|\\"+ end=+"+ end="$" contains=gcmcSpecial
syn region	gcmcCommentL	start="//" skip="\\$" end="$" keepend contains=@gcmcCommentGroup,gcmcSpaceError,@Spell
syn region	gcmcComment	matchgroup=gcmcCommentStart start="/\*" end="\*/" contains=@gcmcCommentGroup,gcmcCommentStartError,gcmcSpaceError,@Spell extend
" keep a // comment separately, it terminates a preproc. conditional
syn match	gcmcCommentError	display "\*/"
syn match	gcmcCommentStartError display "/\*"me=e-1 contained
syn match	gcmcSpaceError	display excludenl "\s\+$"

syn cluster	gcmcParenGroup	contains=gcmcParenError,gcmcSpecial,gcmcCommentSkip,gcmcCommentString,gcmcComment2String,@gcmcCommentGroup,gcmcCommentStartError
",cNumber,cFloat
syn match	gcmcCurlyError	"}"
syn region	gcmcBlock	start="{" end="}" contains=ALLBUT,gcmcBadBlock,gcmcCurlyError,@gcmcParenGroup,gcmcErrInParen,gcmcErrInBracket,@Spell fold
syn region	gcmcBadBlock	keepend start="{" end="}" contained containedin=gcmcParen,gcmcBracket,gcmcBadBlock transparent fold

syn region	gcmcParen	transparent start='(' end=')' end='}'me=s-1 contains=ALLBUT,gcmcBlock,@gcmcParenGroup,gcmcErrInBracket,@Spell
syn match	gcmcParenError	display "[\])]"
syn match	gcmcErrInParen	display contained "[\]{}]\|<%\|%>"
syn region	gcmcBracket	transparent start='\[\|<::\@!' end=']\|:>' end='}'me=s-1 contains=ALLBUT,gcmcBlock,@gcmcParenGroup,gcmcErrInParen,@Spell
syn match	gcmcErrInBracket	display contained "[);{}]\|<%\|%>"

"syn match	cNumber		display contained "\d\+"
syn match	gcmcNumberM	display "\<\d\+\(\s*mm\)\=\>"
syn match	gcmcNumberI	display "\<\d\+\(\s*\(mil\|in\)\)\=\>"
syn match	gcmcNumberD	display "\<\d\+\(\s*deg\)\=\>"
syn match	gcmcNumberR	display "\<\d\+\(\s*rad\)\=\>"
syn match	gcmcNumberC	display "\<\d\+\>"
"floating point number, with dot, optional exponent
syn match	gcmcFloatM	display "\<\d\+\.\d*\(e[-+]\=\d\+\)\=\(\s*mm\)\=\>"
syn match	gcmcFloatI	display "\<\d\+\.\d*\(e[-+]\=\d\+\)\=\(\s*\(mil\|in\)\)\=\>"
syn match	gcmcFloatD	display "\<\d\+\.\d*\(e[-+]\=\d\+\)\=\(\s*deg\)\=\>"
syn match	gcmcFloatR	display "\<\d\+\.\d*\(e[-+]\=\d\+\)\=\(\s*rad\)\=\>"
syn match	gcmcFloatC	display "\<\d\+\.\d*\(e[-+]\=\d\+\)\=\>"
"floating point number, starting with a dot, optional exponent
syn match	gcmcFloatM	display "\<\.\d\+\(e[-+]\=\d\+\)\=\(\s*mm\)\=\>"
syn match	gcmcFloatI	display "\<\.\d\+\(e[-+]\=\d\+\)\=\(\s*\(mil\|in\)\)\=\>"
syn match	gcmcFloatD	display "\<\.\d\+\(e[-+]\=\d\+\)\=\(\s*deg\)\=\>"
syn match	gcmcFloatR	display "\<\.\d\+\(e[-+]\=\d\+\)\=\(\s*rad\)\=\>"
syn match	gcmcFloatC	display "\<\.\d\+\(e[-+]\=\d\+\)\=\>"
"floating point number, without dot, with exponent
syn match	gcmcFloatM	display "\<\d\+e[-+]\=\d\+\(\s*mm\)\=\>"
syn match	gcmcFloatI	display "\<\d\+e[-+]\=\d\+\(\s*\(mil\|in\)\)\=\>"
syn match	gcmcFloatD	display "\<\d\+e[-+]\=\d\+\(\s*deg\)\=\>"
syn match	gcmcFloatR	display "\<\d\+e[-+]\=\d\+\(\s*rad\)\=\>"
syn match	gcmcFloatC	display "\<\d\+e[-+]\=\d\+\>"

syn keyword	gcmcUnitError	mm in mil deg rad

hi def link gcmcNumberM		Number
hi def link gcmcNumberI		Number
hi def link gcmcNumberD		Number
hi def link gcmcNumberR		Number
hi def link gcmcNumberC		Constant
hi def link gcmcFloatM		Float
hi def link gcmcFloatI		Float
hi def link gcmcFloatD		Float
hi def link gcmcFloatR		Float
hi def link gcmcFloatC		Constant
hi def link gcmcConst		Constant
hi def link gcmcStatement	Statement
hi def link gcmcConditional	Conditional
hi def link gcmcRepeat		Repeat
hi def link gcmcTodo		Todo
hi def link gcmcString		String
hi def link gcmcComment		Comment
hi def link gcmcCommentL	gcmcComment
hi def link gcmcCommentStart	gcmcComment
hi def link gcmcCommentError	Error
hi def link gcmcCommentStartError	Error
hi def link gcmcSpecial		SpecialChar
hi def link gcmcSpaceError	gcmcError
hi def link gcmcCurlyError	gcmcError
hi def link gcmcError		Error
hi def link gcmcUnitError	Error
hi def link gcmcParenError	gcmcError
hi def link gcmcErrInParen	gcmcError
hi def link gcmcBuiltin		Function
hi def link gcmcType		Type

let b:current_syntax = "gcmc"
