gcmc - G-code meta-compiler
===========================

Gcmc is a compiler to program G-code, SVG and DXF paths. It uses a procedural
approach, much like C and other modern languages and borrows from several
others as well.

Primitives are scalars, vectors and vector-lists. Scalars and vectors can have
units assigned (mm, inch (in/mil), degrees (deg), radians (rad) or none) which
will be automatically converted upon output.

The gcmc language supports variables and conditional execution using if/else
constructs as well as loop control with foreach/for/while/do-while constructs.
Local functions with arguments can be defined and called including support for
locally scoped variables.

A rich set of built-in functions is provided to manipulate G-code generation,
from simple goto()/move(), which correspond to G0/G1 to more complex vector
handling functions like normalize(), rotate_xy() and length(). Built-in
functions are also provided for G-codes M, T, F and S. Math and other support
functions are also provided as built-ins.

Both SVG and DXF backends support layers and moves may be output on separate
layers if desired. Lasercutters using DXF or SVG may use the layers to separate
the order in which the machine operates.

A support library is provided to supply high-level functions, such as
tool-compensated paths and vectorized arcs, circles and Bezier curves.


Building/Installation
---------------------
See INSTALL


Homepage
--------
http://www.vagrearg.org/content/gcmc

The homepage also has documentation about the language and built-in functions.


Reporting bugs
--------------
If you encounter any bug or need a specific feature, then you can create an
issue at:
  https://gitlab.com/gcmc/gcmc/issues

Alternatively, you can contact me at:
  gcmc@vagrearg.org

For bug-reports, please include an example that triggers the problem.


Examples
--------
Please see the example directory for examples. See INSTALL how to build the
examples.
