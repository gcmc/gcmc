Name: gcmc
Version: 1.9.2
Release: 1
Summary: G-Code Meta Compiler creates G-code, SVG and DXF files from scripts

Group: Development/Tools
License: GPL
URL: http://www.vagrearg.org/content/gcmc
Source0: http://www.vagrearg.org/gcmc/%{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires: bison
BuildRequires: flex

%description
Gcmc is a front-end language for generating G-code, SVG and DXF for CNC mills,
lathes, laser cutters and other numerical controlled machines employing G-code,
SVG or DXF. The language is a context-free grammar created to overcome the
archaic format of G-code programming and aims to be more readable and
understandable. Gcmc makes extensive use of vector mathematics to support the
3D nature of CNC machining.

%prep
%setup -q

%build
./configure --prefix=%{_prefix} --datadir=%{_datadir} --docdir=%{_docdir}/%{name}-%{version}
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%{_bindir}/gcmc
%{_mandir}/man1/gcmc.1*
%{_datadir}/gcmc/*
%doc %{_docdir}/%{name}-%{version}/*

%changelog
* Wed Nov 27 2013 Bertho Stultiens <gcmc@vagrearg.org> 1.4.1-1
- Initial specfile release

