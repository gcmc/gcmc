#!/bin/bash

[ -x ../src/gcmcfont.exe ] && GCMCFONT=../src/gcmcfont.exe
[ -x ../src/gcmcfont ] && GCMCFONT=../src/gcmcfont

# Kill the fixme messages from Wine
export WINEDEBUG=fixme-all

function makeit() {
	base="$(basename "$1" ".glyph")"
	if [ ! -f "${base}.c" -o "$1" -nt "${base}.c" ]; then
		$GCMCFONT -c "${base}" -n "$2" -o "${base}.c" "$1"
	fi
	if [ ! -f "${base}.svg" -o "$1" -nt "${base}.svg" ]; then
		$GCMCFONT -r -s -c "${base}" -n "$2" -o "${base}.svg" "$1"
	fi
}

makeit font_hsans_1.glyph "Hershey Sans regular - single stroke"
makeit font_hsans_1_rs.glyph "Hershey Sans regular - single stroke - reduced strokelength"
makeit font_hsans_2.glyph "Hershey Sans regular - double stroke"
makeit font_hscript_1.glyph "Hershey Script - single stroke"
makeit font_hscript_2.glyph "Hershey Script - double stroke"
makeit font_htimes.glyph "Hershey Times regular"
makeit font_htimes_italic.glyph "Hershey Times italic"
makeit font_htimes_bold.glyph "Hershey Times bold"
makeit font_htimes_italic_bold.glyph "Hershey Times italic bold"

