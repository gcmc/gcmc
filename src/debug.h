/*
 * G-code meta compiler
 *
 * Copyright (C) 2013,2019  B. Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GCMC_DEBUG_H
#define __GCMC_DEBUG_H

typedef enum {
	BREAK_INITIAL,
	BREAK_STEP,
	BREAK_NEXT,
	BREAK_POINT,
} break_et;

extern int debug_signal;	/* Invoked from SIGINT */
extern break_et debug_cond;

void debug_init(void);
void debug_cleanup(void);
void debug_break(node_t *n);

#endif
