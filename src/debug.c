/*
 * G-code meta compiler
 *
 * Copyright (C) 2019  B. Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>
#include <limits.h>

#ifdef HAVE_READLINE
#include <readline/readline.h>
#endif
#ifdef HAVE_HISTORY
#include <readline/history.h>
#endif

#include "grammartypes.h"
#include "debug.h"
#include "utils.h"
#include "value.h"
#include "variable.h"
#include "interpreter.h"

int debug_signal = 0;	/* Invoked from SIGINT */
break_et debug_cond = BREAK_INITIAL;

#ifndef USE_DEBUGGER
/* An empty stubs without a debugger */
void debug_break(node_t *n)
{
}

void debug_init(void)
{
}

void debug_cleanup(void)
{
}

#else /* We have a debugger */

#ifdef HAVE_HISTORY
static char *histfname;
#endif

static void debug_print_ast(const node_t *head, int lines);

static int cmd_dot(const char **toks, const node_t *n);
static int cmd_help(const char **toks, const node_t *n);
static int cmd_exit(const char **toks, const node_t *n);
static int cmd_bt(const char **toks, const node_t *n);
static int cmd_break(const char **toks, const node_t *n);
static int cmd_unbreak(const char **toks, const node_t *n);
static int cmd_list(const char **toks, const node_t *n);
static int cmd_locals(const char **toks, const node_t *n);
static int cmd_show(const char **toks, const node_t *n);
static int cmd_step(const char **toks, const node_t *n);
static int cmd_next(const char **toks, const node_t *n);
static int cmd_cont(const char **toks, const node_t *n);
static int cmd_globals(const char **toks, const node_t *n);
static int cmd_vars(const char **toks, const node_t *n);
static int cmd_consts(const char **toks, const node_t *n);
static char **cmpl_help(const char **toks);
static char **cmpl_list(const char **toks);
static char **cmpl_show(const char **toks);
static char **cmpl_break(const char **toks);
static char **cmpl_unbreak(const char **toks);
static char **cmpl_none(const char **toks);

typedef int (*cmdfunc_t)(const char **tokens, const node_t *node);
typedef char **(*cmplfunc_t)(const char **tokens);
typedef struct {
	const char *name;
	cmdfunc_t cmdfunc;	/* Command function */
	cmplfunc_t cmplfunc;	/* Completer function */
	const char *helphelp;	/* Full help message */
	const char *help;	/* Brief help message  */
} dbgcmd_t;

static node_t **breakpoints;
static int nbreakpoints;
static int nabreakpoints;
static unsigned breakpointid;

static const char help_dot[] =
	"Syntax:\n"
	"  .\n"
	"Print the current line of code to be executed from the internal AST\n"
	"representation of the script. The AST differs from the script source and\n"
	"it denotes where the possible breakpoint are. The AST representation is\n"
	"statement based.\n"
	"See also 'list' command to show the listing based on source.\n"
	;
static const char help_help[] =
	"Syntax:\n"
	"  help [cmd]\n"
	"  ? [cmd]\n"
	"Help on help helps you to help yourself.\n"
	"There are helping hands when you type 'help' with any command that might\n"
	"be of interest. Consider being helped by help and do not let yourself be\n"
	"helped by thinking, please. Help is always provided to those who seek\n"
	"help typing an existing command after help.\n"
	"By the way, did you already notice the help of the program, providing\n"
	"context sensitive tab-completion and full command history?\n"
	"Commands may be abbreviated if the command is uniquely defined by the\n"
	"entered prefix (e.g. 'quit' may be shortened to 'q')\n"
	;
static const char help_exit[] =
	"Syntax\n"
	"  exit\n"
	"  quit\n"
	"You are leaving (exit), giving up (quit) or need to concentrate on fixing\n"
	"your code other places (either exit or quit). These are the commands that\n"
	"will provide all those options for you. If you give up, please consider\n"
	"asking a question using old-style email, if you still have such archaic\n"
	"pleasure at your fingertips.\n"
	;
static const char help_locals[] =
	"Syntax\n"
	"  locals [n]\n"
	"Display all variables in the local scope of the current function. The optional\n"
	"argument 'n' is the scope level to display. The active local scope level is\n"
	"zero (0), which is the default if 'n' is omitted. The level 1 is one scope up\n"
	"the call-stack, 2 the second level, etc..\n"
	;
static const char help_globals[] =
	"Syntax\n"
	"  globals\n"
	"  vars\n"
	"  consts\n"
	"Show all variables in the global scope of the entire script, which are visible\n"
	"at this time (including gcmc internal variables). Only variables, which have\n"
	"been defined, are visible. Your script delays creation of global variables,\n"
	"depending on the code order in your script.\n"
	"- 'globals' - shows all available global variables\n"
	"- 'vars'    - shows all mutable global variables\n"
	"- 'consts'  - shows all constant global variables\n"
	;
static const char help_step[] =
	"Syntax\n"
	"  step\n"
	"Step one line of code. Function calls are entered when called.\n"
	;
static const char help_next[] =
	"Syntax\n"
	"  next\n"
	"Next one line of code. Function calls are executed but stepped over. Breakpoint\n"
	"are honored if hit in a called function.\n"
	;
static const char help_cont[] =
	"Syntax\n"
	"  cont\n"
	"Continue the program. Execution is suspended when a breakpoint is encountered or\n"
	"when the program ends. Longer running programs may be suspended by ^C keyboard\n"
	"interrupt."
	;
static const char help_break[] =
	"Syntax\n"
	"  break [file line]\n"
	"List all breakpoints without argument. Otherwise, set a breakpoint in 'file'\n"
	"at 'line'. If the linenr is not found to be breakable, then the next higher\n"
	"linenr is searched in the possible list of breakpoints and set. A message is\n"
	"emitted if no matching line can be found\n"
	;
static const char help_unbreak[] =
	"Syntax\n"
	"  unbreak id\n"
	"Unset the breakpoint with 'id'. The identification is a number from the list\n"
	"of breakpoints, which can be obtained from the break command.\n"
	;
static const char help_bt[] =
	"Syntax\n"
	"  bt\n"
	"  backtrace\n"
	"Show the call-stack from current position in the code.\n"
	;
static const char help_list[] =
	"Syntax\n"
	"  list [file [line]]\n"
	"Show the (real) source of 'file' starting from 'line'. The listing starts at\n"
	"the first line of 'file' if the 'line' argument is omitted. The listing starts\n"
	"at the current execution position (based on the AST) if both arguments are\n"
	"omitted.\n"
	"See also '.' command to show the listing based on AST.\n"
	;
static const char help_show[] =
	"Syntax\n"
	"  show var\n"
	"  print var\n"
	"Print the content of a single variable 'var'. Expressions are not (yet)\n"
	"supported.\n"
	;

static const dbgcmd_t commands[] = {
	{ ".",		cmd_dot,	cmpl_none,	help_dot,	".          - Print current code line from AST" },
	{ "?",		cmd_help,	cmpl_help,	help_help,	"? [cmd]    - This help screen" },
	{ "help",	cmd_help,	cmpl_help,	help_help,	"help [cmd] - This help screen" },
	{ "exit",	cmd_exit,	cmpl_none,	help_exit,	"exit       - Exit gcmc" },
	{ "quit",	cmd_exit,	cmpl_none,	help_exit,	"quit       - Exit gcmc" },
	{ "list",	cmd_list,	cmpl_list,	help_list,	"list [file [line]] - List source" },
	{ "locals",	cmd_locals,	cmpl_none,	help_locals,	"locals [n] - View all local variables at scope -n" },
	{ "globals",	cmd_globals,	cmpl_none,	help_globals,	"globals    - View all global variables" },
	{ "vars",	cmd_vars,	cmpl_none,	help_globals,	"vars       - View all globals non-const variables " },
	{ "consts",	cmd_consts,	cmpl_none,	help_globals,	"consts     - View all globals const variables " },
	{ "print",	cmd_show,	cmpl_show,	help_show,	"print var  - Print variable" },
	{ "show",	cmd_show,	cmpl_show,	help_show,	"show var   - Print variable" },
	{ "cont",	cmd_cont,	cmpl_none,	help_cont,	"cont       - Continue execution" },
	{ "step",	cmd_step,	cmpl_none,	help_step,	"step       - Single step, into calls" },
	{ "next",	cmd_next,	cmpl_none,	help_next,	"next       - Next line, step over calls" },
	{ "bt",		cmd_bt,		cmpl_none,	help_bt,	"bt         - Backtrace call-stack" },
	{ "backtrace",	cmd_bt,		cmpl_none,	help_bt,	"backtrace  - Backtrace call-stack" },
	{ "break",	cmd_break,	cmpl_break,	help_break,	"break [file line] - List or set breakpoint" },
	{ "unbreak",	cmd_unbreak,	cmpl_unbreak,	help_unbreak,	"unbreak id - Remove breakpoint" },
	{ NULL,		NULL,		NULL,		NULL,		NULL }
};

static char **tokenize(char *cptr)
{
	char **tokens = NULL;
	int ntokens = 0;
	int natokens = 0;

	cptr = strdup(cptr);	/* Create local copy */

	/* Strip leading whitespace while preserving pointer to start (for free()) */
	while(*cptr && strchr(" \t", *cptr)) {
		memmove(cptr, cptr+1, strlen(cptr));
	}
	testalloc((void **)&tokens, ntokens, &natokens, sizeof(tokens[0]));
	tokens[ntokens++] = cptr;	/* First token is the line */

	for(; *cptr; cptr++) {
		if('\\' == *cptr) {
			if(cptr[1])	/* Need one more character after backslash */
				cptr++;
			continue;	/* Ignore the trailing character of a backslash */
		}
		if(strchr(" \t", *cptr)) {
			*cptr++ = '\0';
			while(*cptr && strchr(" \t", *cptr)) {
				*cptr++ = '\0';
			}
			if(!*cptr)
				break;
			testalloc((void **)&tokens, ntokens, &natokens, sizeof(tokens[0]));
			tokens[ntokens++] = cptr;
		}
	}
	testalloc((void **)&tokens, ntokens, &natokens, sizeof(tokens[0]));
	tokens[ntokens] = NULL;	/* Ensure teminating null */
	return tokens;
}

static char **cmpl_help(const char **toks)
{
	char **matches = NULL;
	int nmatches = 0;
	int namatches = 0;
	const char *partial = toks[1] ? toks[1] : "";
	int n = strlen(partial);
	int i;

	/* Check if the argument already is complete */
	for(i = 0; commands[i].name; i++) {
		if(!strcmp(partial, commands[i].name))
			return NULL;	/* Complete -> no further match */
	}

	/* First entry is longest common prefix */
	testalloc((void **)&matches, nmatches, &namatches, sizeof(matches[0]));
	matches[nmatches++] = NULL;

	/* Accumulate the partials */
	for(i = 0; commands[i].name; i++) {
		if(!strncmp(partial, commands[i].name, n)) {
			testalloc((void **)&matches, nmatches, &namatches, sizeof(matches[0]));
			matches[nmatches++] = strdup(commands[i].name);
		}
	}
	testalloc((void **)&matches, nmatches, &namatches, sizeof(matches[0]));
	matches[nmatches++] = NULL;	/* Terminate list */
	return matches;
}

static char **cmpl_list(const char **toks)
{
	char **matches = NULL;
	int nmatches = 0;
	int namatches = 0;
	const char *partial = toks[1] ? toks[1] : "";
	int n = strlen(partial);
	int i;
	int havefile = 0;

	/* First entry is longest common prefix */
	testalloc((void **)&matches, nmatches, &namatches, sizeof(matches[0]));
	matches[nmatches++] = NULL;

	/* Check if we have a full name already */
	for(i = 0; i < ndbgfiles; i++) {
		if(!strcmp(partial, dbgfiles[i].shortname) || !strcmp(partial, dbgfiles[i].filename)) {
			havefile = 1;
			break;
		}
	}

	if(havefile) {
		char buf[32];
		const dbgfile_t *df = &dbgfiles[i];
		partial = toks[2] ? toks[2] : "";
		n = strlen(partial);
		for(i = 0; i < df->nlines; i++) {
			sprintf(buf, "%d", i+1);
			if(!strncmp(partial, buf, n)) {
				testalloc((void **)&matches, nmatches, &namatches, sizeof(matches[0]));
				matches[nmatches++] = strdup(buf);
			}
		}
	} else {
		/* Accumulate the partials */
		for(i = 0; i < ndbgfiles; i++) {
			if(!dbgfiles[i].nnodes)
				continue;
			if(!strncmp(partial, dbgfiles[i].shortname, n)) {
				testalloc((void **)&matches, nmatches, &namatches, sizeof(matches[0]));
				matches[nmatches++] = strdup(dbgfiles[i].shortname);
			}
			if(!strncmp(partial, dbgfiles[i].filename, n)) {
				testalloc((void **)&matches, nmatches, &namatches, sizeof(matches[0]));
				matches[nmatches++] = strdup(dbgfiles[i].filename);
			}
		}
	}

	testalloc((void **)&matches, nmatches, &namatches, sizeof(matches[0]));
	matches[nmatches++] = NULL;	/* Terminate list */
	return matches;
}

static void free_charlist(char **l)
{
	char **cpptr;
	if(!l)
		return;
	for(cpptr = l; *cpptr; cpptr++) {
		if(*cpptr)
			free(*cpptr);
	}
	free(l);
}

static char **cmpl_show(const char **toks)
{
	char **matches = NULL;
	int nmatches = 0;
	int namatches = 0;
	char **lvars;
	char **gvars;
	char **cpptr;
	const char *partial = toks[1] ? toks[1] : "";
	int n = strlen(partial);

	lvars = variable_locals_list();
	gvars = variable_globals_list();

	/* Check if already fully matched if something was typed */
	if(*partial) {
		for(cpptr = lvars; lvars && *cpptr; cpptr++) {
			if(!strcmp(*cpptr, partial)) {
				free_charlist(lvars);
				free_charlist(gvars);
				return NULL;
			}
		}
		for(cpptr = gvars; gvars && *cpptr; cpptr++) {
			if(!strcmp(*cpptr, partial)) {
				free_charlist(lvars);
				free_charlist(gvars);
				return NULL;
			}
		}
	}

	/* First entry is longest common prefix */
	testalloc((void **)&matches, nmatches, &namatches, sizeof(matches[0]));
	matches[nmatches++] = NULL;

	/* Add local visible vars */
	for(cpptr = lvars; lvars && *cpptr; cpptr++) {
		if(!strncmp(partial, *cpptr, n)) {
			testalloc((void **)&matches, nmatches, &namatches, sizeof(matches[0]));
			matches[nmatches++] = *cpptr;
			*cpptr = NULL;
		}
	}

	/* Add global visible vars */
	for(cpptr = gvars; gvars && *cpptr; cpptr++) {
		if(!strncmp(partial, *cpptr, n)) {
			testalloc((void **)&matches, nmatches, &namatches, sizeof(matches[0]));
			matches[nmatches++] = *cpptr;
			*cpptr = NULL;
		}
	}
	free_charlist(lvars);
	free_charlist(gvars);

	testalloc((void **)&matches, nmatches, &namatches, sizeof(matches[0]));
	matches[nmatches++] = NULL;	/* Terminate list */
	return matches;
}

static char **cmpl_break(const char **toks)
{
	char **matches = NULL;
	int nmatches = 0;
	int namatches = 0;
	const char *partial = toks[1] ? toks[1] : "";
	int n = strlen(partial);
	int i;
	int havefile = 0;

	/* First entry is longest common prefix */
	testalloc((void **)&matches, nmatches, &namatches, sizeof(matches[0]));
	matches[nmatches++] = NULL;

	/* Check if we have a full name already */
	for(i = 0; i < ndbgfiles; i++) {
		if(!strcmp(partial, dbgfiles[i].shortname)) {
			havefile = 1;
			break;
		}
	}

	if(havefile) {
		char buf[32];
		const dbgfile_t *df = &dbgfiles[i];
		partial = toks[2] ? toks[2] : "";
		n = strlen(partial);
		for(i = 0; i < df->nnodes; i++) {
			sprintf(buf, "%d", df->nodes[i]->linenr);
			if(!strncmp(partial, buf, n)) {
				testalloc((void **)&matches, nmatches, &namatches, sizeof(matches[0]));
				matches[nmatches++] = strdup(buf);
			}
		}
	} else {
		/* Accumulate the partials */
		for(i = 0; i < ndbgfiles; i++) {
			if(!dbgfiles[i].nnodes)
				continue;
			if(!strncmp(partial, dbgfiles[i].shortname, n)) {
				testalloc((void **)&matches, nmatches, &namatches, sizeof(matches[0]));
				matches[nmatches++] = strdup(dbgfiles[i].shortname);
			}
		}
	}

	testalloc((void **)&matches, nmatches, &namatches, sizeof(matches[0]));
	matches[nmatches++] = NULL;	/* Terminate list */
	return matches;
}

static char **cmpl_unbreak(const char **toks)
{
	char **matches = NULL;
	int nmatches = 0;
	int namatches = 0;
	const char *partial = toks[1] ? toks[1] : "";
	int n = strlen(partial);
	char buf[32];
	int i;

	/* No completion if no breakpoints */
	if(!nbreakpoints)
		return NULL;

	/* First entry is longest common prefix */
	testalloc((void **)&matches, nmatches, &namatches, sizeof(matches[0]));
	matches[nmatches++] = NULL;

	for(i = 0; i < nbreakpoints; i++) {
		sprintf(buf, "%u", breakpoints[i]->debug);
		if(!strncmp(partial, buf, n)) {
			testalloc((void **)&matches, nmatches, &namatches, sizeof(matches[0]));
			matches[nmatches++] = strdup(buf);
		}
	}

	testalloc((void **)&matches, nmatches, &namatches, sizeof(matches[0]));
	matches[nmatches++] = NULL;	/* Terminate list */
	return matches;
}

static char **cmpl_none(const char **toks)
{
	(void)toks;
	return NULL;
}

static const dbgcmd_t *find_cmd(const char *s, int fullmatch)
{
	int i;
	if(!s)
		return NULL;

	if(fullmatch) {
		for(i = 0; commands[i].name; i++) {
			if(!strcmp(commands[i].name, s))
				return &commands[i];
		}
		return NULL;
	} else {
		const dbgcmd_t *cmd = NULL;
		int n = strlen(s);
		for(i = 0; commands[i].name; i++) {
			if(!strncmp(commands[i].name, s, n)) {
				if(cmd)
					return NULL;	/* Command is ambiguous */
				cmd = &commands[i];
			}
		}
		return cmd;
	}
}

#ifdef HAVE_READLINE
static void common_prefix(char **matches)
{
	int i;
	int n = INT_MAX;

	if(!matches)
		return;

	if(!matches[1]) {
		/* If no strings, prefix is empty too */
		matches[0] = strdup("");
		return;
	}

	/* Find shortest string */
	for(i = 1; matches[i]; i++) {
		int l = strlen(matches[i]);
		if(l < n)
			n = l;
	}

	if(!n) {
		/* If shortest string is empty, prefix is empty too */
		matches[0] = strdup("");
		return;
	} else {
		matches[0] = calloc(n+1, sizeof(*(matches[0])));
	}
	n = 0;
	while(1) {
		char current = matches[1][n];
		if(!current)
			return;
		for(i = 2; matches[i]; i++) {
			if(matches[i][n] != current) {
				return;
			}
		}
		matches[0][n++] = current;
	}
}

static char **debug_completion(const char *text, int start, int end)
{
	char **matches = NULL;
	int nmatches = 0;
	int namatches = 0;
	char **tokens = tokenize(rl_line_buffer);
	const dbgcmd_t *cmd;

	rl_attempted_completion_over = 1;	/* No filename completion, ever */

	if((cmd = find_cmd(tokens[0], 1))) {
		/* call completer for relevant command */
		matches = cmd->cmplfunc((const char **)tokens);
	} else {
		int i;
		int n = strlen(tokens[0]);
		/* First entry is longest common prefix */
		testalloc((void **)&matches, nmatches, &namatches, sizeof(matches[0]));
		matches[nmatches++] = NULL;
		/* Other matches come from the commands */
		for(i = 0; commands[i].name; i++) {
			if(!strncmp(tokens[0], commands[i].name, n)) {
				testalloc((void **)&matches, nmatches, &namatches, sizeof(matches[0]));
				matches[nmatches++] = strdup(commands[i].name);
			}
		}
		testalloc((void **)&matches, nmatches, &namatches, sizeof(matches[0]));
		matches[nmatches++] = NULL;	/* Terminate list */
	}

	common_prefix(matches);
	free(tokens[0]);
	free(tokens);
	return matches;
}
#endif

#ifndef HAVE_READLINE
static char rl_line_buffer[1024];
static char *readline(const char *prompt)
{
	printf("%s", prompt);
	fflush(stdout);
	return fgets(rl_line_buffer, sizeof(rl_line_buffer), stdin);
}
#endif

void debug_break(node_t *n)
{
	if(debug_cond == BREAK_POINT) {
		if(n->debug)
			printf("Breakpoint %u at %s:%d\n", n->debug, n->filename, n->linenr);
		else
			printf("Interrupt at %s:%d\n", n->filename, n->linenr);
	}
	debug_print_ast(n, 1);
	while(1) {
		char *line = readline ("GcmcDbg: ");
		char **tokens;
		const dbgcmd_t *cmd;
		static char **oldtokens = NULL;
		static const dbgcmd_t *oldcmd = NULL;
		int rv = 0;

		if(!line) {	/* On ^D -> EOF means exit */
			if(oldtokens) {
				free(oldtokens[0]);
				free(oldtokens);
			}
			printf("exit\n");
			cmd_exit(NULL, n);
		}

		tokens = tokenize(line);
		if(*tokens[0]) {	/* Must have something on the line or it can be ignored */
			cmd = find_cmd(tokens[0], 0);

			if(!cmd) {
				fprintf (stderr, "%s: No such command for gcmc debugger.\n", tokens[0]);
				if(oldtokens) {
					free(oldtokens[0]);
					free(oldtokens);
					oldtokens = NULL;
					oldcmd = NULL;
				}
			} else {
#ifdef HAVE_HISTORY
				HIST_ENTRY *he = history_get(history_length - 1);
				if(!he || strcmp(line, he->line))
					add_history(line);
#endif
				rv = cmd->cmdfunc((const char **)tokens, n);
				if(oldtokens) {
					free(oldtokens[0]);
					free(oldtokens);
				}
				oldtokens = tokens;
				tokens = NULL;
				oldcmd = cmd;
			}
		} else if(oldtokens) {
			/* The 'list' command is different because we want to continue the listing, not
			 * just repeat the same. We delete the subsequent tokens and repeat without
			 * arguments.
			 */
			if(!strncmp("li", oldtokens[0], 2)) {
				char **cpptr = &oldtokens[1];
				while(*cpptr) {
					*cpptr++ = NULL;
				}
			}
			rv = oldcmd->cmdfunc((const char **)oldtokens, n);
		}

		free(line);
		if(tokens) {
			free(tokens[0]);
			free(tokens);
		}
		if(rv)
			break;
	}

	debug_signal = 0;
}

void debug_init(void)
{
#ifdef HAVE_HISTORY
#define GCMCHISTPATH	"/.gcmcdbghst"
	char *home = getenv("HOME");
	if(!home || !*home)
		home = ".";
	histfname = malloc(strlen(home) + sizeof(GCMCHISTPATH) + 1);
	strcpy(histfname, home);
	strcat(histfname, GCMCHISTPATH);
#undef GCMCHISTPATH
#endif

#ifdef HAVE_READLINE
	rl_readline_name = "gcmc";				/* Allow conditional parsing of the ~/.inputrc file */
	rl_attempted_completion_function = debug_completion;	/* We want to complete first */
	rl_basic_word_break_characters = strdup(" \t:");	/* Spaced, tabs and colon */
#endif
#ifdef HAVE_HISTORY
	using_history();
	read_history(histfname);
	stifle_history(2500);	/* That should be enough for now */
#endif
}

void debug_cleanup(void)
{
#ifdef HAVE_HISTORY
	write_history(histfname);
#endif
}

static int cmd_dot(const char **toks, const node_t *n)
{
	(void)toks;
	debug_print_ast(n, 1);
	return 0;
}

static int cmd_help(const char **toks, const node_t *n)
{
	const dbgcmd_t *dc = find_cmd(toks[1], 1);
	if(dc) {
		printf("%s", dc->helphelp ? dc->helphelp : "<help missing>\n");
	} else {
		for(dc = &commands[0]; dc->name; dc++) {
			printf("%-10s%s\n", dc->name, dc->help);
		}
		printf("\n");
	}
	return 0;
}

static int cmd_bt(const char **toks, const node_t *n)
{
	callstack_print();
	return 0;
}

static int cmd_exit(const char **toks, const node_t *n)
{
	debug_cleanup();	/* Or we do not save out history */
	exit(0);
	return 0;
}

static int cmd_list(const char **toks, const node_t *n)
{
	static const dbgfile_t *df = NULL;
	static int tag;
	int i;
#ifdef HAVE_READLINE
	int rows, cols;
	rl_get_screen_size(&rows, &cols);
#else
	int rows = 24;
#endif

	if(!toks[1]) {
		/* No filename given, use current node */
		if(!df) {
			/* Translate node into debug file reference */
			for(i = 0; i < ndbgfiles; i++) {
				if(!strcmp(dbgfiles[i].filename, n->filename)) {
					df = &dbgfiles[i];
					tag = n->linenr - 1;
					break;
				}
			}
			if(!df) {
				printf("list: error: cannot find source for file '%s'.\n", n->filename);
				return 0;
			}
		}
	} else {
		/* Find the file in the list */
		for(i = 0; i < ndbgfiles; i++) {
			if(!strcmp(dbgfiles[i].filename, toks[1])) {
				df = &dbgfiles[i];
				tag = 0;
				break;
			}
		}
		/* Try to find the name in the shortlist */
		for(i = 0; !df && i < ndbgfiles; i++) {
			if(!strcmp(dbgfiles[i].shortname, toks[1])) {
				df = &dbgfiles[i];
				tag = 0;
				break;
			}
		}
		if(!df) {
			printf("list: error: cannot find source for file '%s'.\n", toks[1]);
			return 0;
		}
		if(toks[2]) {
			tag = strtol(toks[2], NULL, 0);
			if(tag <= 0) {
				printf("list: error: invalid line number for file '%s' (1...%d).\n", toks[1], df->nlines);
				return 0;
			}
			tag--;
		}
	}

	for(i = 0; i < rows/2; i++) {
		if(tag >= df->nlines) {
			printf("---<EOF>---\n");
			df = NULL;
			return 0;
		}
		printf("% 4d %s\n", tag+1, df->lines[tag]);
		tag++;
	}
	return 0;
}

static int cmd_locals(const char **toks, const node_t *n)
{
	int lvl = 0;
	if(toks[1]) {
		lvl = strtol(toks[1], NULL, 0);
	}
	if(lvl < 0) {
		printf("Local scope level must be >= 0\n");
		return 0;
	}
	variable_print_locals(lvl);
	return 0;
}

static int cmd_globals(const char **toks, const node_t *n)
{
	variable_print_globals(VAR_VAR | VAR_CONST);
	return 0;
}

static int cmd_vars(const char **toks, const node_t *n)
{
	variable_print_globals(VAR_VAR);
	return 0;
}

static int cmd_consts(const char **toks, const node_t *n)
{
	variable_print_globals(VAR_CONST);
	return 0;
}

static int cmd_show(const char **toks, const node_t *n)
{
	wchar_t *wname;
	size_t nc;

	if(!toks[1] || !*toks[1]) {
		printf("show: error: must have a valid variable name as argument\n");
		return 0;
	}
	nc = mbstowcs(NULL, toks[1], 0);
	if(nc == (size_t)-1) {
		printf("show: error: \"%s\" includes invalid byte-sequence\n", toks[1]);
		return 0;
	}
	wname = malloc((nc+1) * sizeof(*wname));
	mbstowcs(wname, toks[1], nc+1);
	variable_print(wname);
	free(wname);
	return 0;
}

static int cmd_cont(const char **toks, const node_t *n)
{
	callstack_debug_cond(BREAK_POINT);
	debug_cond = BREAK_POINT;
	return 1;
}

static int cmd_step(const char **toks, const node_t *n)
{
	callstack_debug_cond(BREAK_STEP);
	debug_cond = BREAK_STEP;
	return 1;
}

static int cmd_next(const char **toks, const node_t *n)
{
	callstack_debug_cond(BREAK_NEXT);
	debug_cond = BREAK_NEXT;
	return 1;
}

static unsigned breakpoint_add(node_t *n)
{
	if(n->debug)
		return n->debug;
	testalloc((void **)&breakpoints, nbreakpoints, &nabreakpoints, sizeof(breakpoints[0]));
	breakpoints[nbreakpoints++] = n;
	n->debug = ++breakpointid;
	return 0;
}

static int breakpoint_del(unsigned id)
{
	int i;
	for(i = 0; i < nbreakpoints; i++) {
		if(breakpoints[i]->debug == id) {
			breakpoints[i]->debug = 0;
			nbreakpoints--;
			breakpoints[i] = breakpoints[nbreakpoints];
			breakpoints[nbreakpoints] = NULL;
			return 1;
		}
	}
	return 0;
}

static int cmd_break(const char **toks, const node_t *n)
{
	int i;
	if(!toks[1]) {
		/* break without args --> list breakpoints */
		if(!nbreakpoints) {
			printf("No breakpoints defined\n");
		} else {
			for(i = 0; i < nbreakpoints; i++) {
				printf("Breakpoint %u: ", breakpoints[i]->debug);
				debug_print_ast(breakpoints[i], 1);
			}
		}
	} else {
		/* break with args set a new breakpoint */
		int ln;
		const dbgfile_t *df = NULL;
		if(!toks[2]) {
			printf("break: error: missing line number\n");
			return 0;
		}
		ln = strtol(toks[2], NULL, 0);
		if(ln <= 0) {
			printf("break: error: line numbers start from 1\n");
			return 0;
		}

		for(i = 0; i < ndbgfiles; i++) {
			if(!strcmp(toks[1], dbgfiles[i].shortname)) {
				df = &dbgfiles[i];
				break;
			}
		}
		if(!df) {
			printf("break: error: file '%s' not found\n", toks[1]);
			return 0;
		}
		if(ln > df->nodes[df->nnodes-1]->linenr) {
			printf("break: error: line number extends beyond file.\n");
			return 0;
		}
		for(i = 0; i < df->nnodes; i++) {
			if(ln <= df->nodes[i]->linenr) {
				if(df->nodes[i]->type == NT_FUNCTION) {
					printf("break: error: Breakpoint at function declaration has no effect. It must be set in the definition (function body).\n");
					return 0;
				}
				if(!breakpoint_add((node_t *)df->nodes[i]))
					printf("Breakpoint %u set at %s:%d\n", df->nodes[i]->debug, df->filename, df->nodes[i]->linenr);
				else
					printf("Breakpoint %u was already set at %s:%d\n", df->nodes[i]->debug, df->filename, df->nodes[i]->linenr);
				return 0;
			}
		}
		printf("break: error: No breakable line found at %d.\n", ln);
	}
	return 0;
}

static int cmd_unbreak(const char **toks, const node_t *n)
{
	unsigned bp;
	if(!nbreakpoints) {
		printf("unbreak: error: there are no breakpoints to remove.\n");
		return 0;
	}
	if(!toks[1]) {
		printf("unbreak: error: must specify which breakpoint to remove.\n");
		return 0;
	}
	bp = strtoul(toks[1], NULL, 0);
	if(bp > breakpointid) {
		printf("unbreak: error: breakpoint id is larger than possible choices.\n");
		return 0;
	}
	if(!breakpoint_del(bp)) {
		printf("unbreak: error: breakpoint %u was not defined.\n", bp);
	} else {
		printf("Breakpoint %u removed.\n", bp);
	}
	return 0;
}

static void print_pfx(const node_t *n, int level, int space)
{
	static const char pfx[80+1] = "                                                                                ";
	const char *cptr = strrchr(n->filename, '/');
	if(!cptr)
		cptr = n->filename;
	else 
		cptr++;
	printf("%.16s:%*d %.*s", cptr, 4+space, n->linenr, level*2, pfx);
}

static void print_ast(const node_t *head, int idx, int level, int lines)
{
	const node_t *n, *next;
	int i;

	if(!head)
		return;


	for(n = head; n; n = next) {
		next = n->next;

		/* Hide internal setup */
		if(!strcmp(n->filename, "<gcmc-internal constants>"))
			continue;

		switch(n->type) {
		case NT_EXPRLIST:
			for(i = 0; i < n->nlist.n; i++) {
				print_ast(n->nlist.nodes[i], idx+1, level, lines);
				if(i < n->nlist.n-1)
					printf(", ");
			}
			break;
		case NT_ARGLIST:
			for(i = 0; i < n->alist.n; i++) {
				if(n->alist.args[i].isref)
					printf("&");
				printf("%ls", n->alist.args[i].id);
				if(n->alist.args[i].expr) {
					printf(" = ");
					print_ast(n->alist.args[i].expr, idx+1, level, lines);
				}
				if(i < n->alist.n-1)
					printf(", ");
			}
			break;
		case NT_RETURN:
			print_pfx(n, level, 0);
			printf("return");
			print_ast(n->eref, idx+1, level, lines);
			printf(";\n");
			break;
		case NT_LOCAL:
			print_pfx(n, level, 0);
			printf("local %ls", n->lvar.id);
			if(n->lvar.init)
				printf(" = ");
			print_ast(n->lvar.init, idx+1, level, lines);
			printf(";\n");
			break;
		case NT_CONST:
			print_pfx(n, level, 0);
			printf("const %ls = ", n->cvar.id);
			print_ast(n->cvar.init, idx+1, level, lines);
			printf(";\n");
			break;
		case NT_BREAK:
			print_pfx(n, level, 0);
			printf("break;\n");
			break;
		case NT_CONTINUE:
			print_pfx(n, level, 0);
			printf("continue;\n");
			break;
		case NT_INVALID:
			printf("Invalid node:"); // FIXME: should never happen
			break;
		case NT_DEBUG:
			if(lines <= 0)
				return;
			lines--;
			printf(".");
			break;
		case NT_EXPR:
			if(!idx)
				print_pfx(n, level, 0);
			if(n->expr.inparen)
				printf("(");
			if(n->expr.op == OP_CONDEXPR) {
				print_ast(n->expr.cond, idx+1, level, lines);
				printf(" ? ");
				print_ast(n->expr.left, idx+1, level, lines);
				printf(" : ");
				print_ast(n->expr.right, idx+1, level, lines);
			} else if(n->expr.op == OP_INDEX) {
				print_ast(n->expr.left, idx+1, level, lines);
				printf("[");
				print_ast(n->expr.right, idx+1, level, lines);
				printf("]");
			} else if(n->expr.op == OP_INDEXID) {
				print_ast(n->expr.left, idx+1, level, lines);
				printf(".");
				assert(n->expr.right->type == NT_EXPR);
				assert(n->expr.right->expr.op == OP_INT);
				assert(n->expr.right->expr.i >= 0);
				assert(n->expr.right->expr.i < 9);
				printf("%c", "xyzabcuvw"[n->expr.right->expr.i]);
			} else if(n->expr.op == OP_CALL) {
				printf("%ls(", n->expr.id);
				print_ast(n->expr.args, idx+1, level, lines);
				printf(")");
			} else {
				print_ast(n->expr.left, idx+1, level, lines);
				switch(n->expr.op) {
				case OP_CONDEXPR: /* Handled above */
				case OP_INDEX:
				case OP_INDEXID:
				case OP_CALL:
				case OP_NULL:	break;
				case OP_ADD:	printf(" + "); break;
				case OP_ADDOR:	printf(" +| "); break;
				case OP_SUB:	printf(" - "); break;
				case OP_SUBOR:	printf(" -| "); break;
				case OP_MUL:	printf(" * "); break;
				case OP_DIV:	printf(" / "); break;
				case OP_MOD:	printf(" %% "); break;
				case OP_LOR:	printf(" || "); break;
				case OP_LAND:	printf(" && "); break;
				case OP_BOR:	printf(" | "); break;
				case OP_BXOR:	printf(" ^ "); break;
				case OP_BAND:	printf(" & "); break;
				case OP_BNOT:	printf(" ~ "); break;
				case OP_EQ:	printf(" == "); break;
				case OP_NE:	printf(" != "); break;
				case OP_GT:	printf(" > "); break;
				case OP_LT:	printf(" < "); break;
				case OP_GE:	printf(" >= "); break;
				case OP_LE:	printf(" <= "); break;
				case OP_SHL:	printf(" << "); break;
				case OP_SHR:	printf(" >> "); break;
				case OP_NOT:	printf(" ! "); break;
				case OP_INT:	printf("%d%s", n->expr.i, unitshortname(n->expr.unit)); break;
				case OP_FLOAT:	printf("%f%s", n->expr.d, unitshortname(n->expr.unit)); break;
				case OP_ASSIGN:		printf(" = "); break;
				case OP_ADDASSIGN:	printf(" += "); break;
				case OP_ADDORASSIGN:	printf(" +|= "); break;
				case OP_SUBASSIGN:	printf(" -= "); break;
				case OP_SUBORASSIGN:	printf(" -|= "); break;
				case OP_MULASSIGN:	printf(" *= "); break;
				case OP_DIVASSIGN:	printf(" /= "); break;
				case OP_MODASSIGN:	printf(" %%= "); break;
				case OP_SHLASSIGN:	printf(" <<= "); break;
				case OP_SHRASSIGN:	printf(" >>= "); break;
				case OP_BORASSIGN:	printf(" |= "); break;
				case OP_BANDASSIGN:	printf(" &= "); break;
				case OP_BXORASSIGN:	printf(" ^= "); break;
				case OP_STRING:		printf("\"%ls\"", n->expr.str.chs); break;
				case OP_DEREF:		printf("%ls", n->expr.id); break;
				case OP_PREINC:		printf("++"); break;
				case OP_PREDEC:		printf("--"); break;
				case OP_POSTINC:	printf("++"); break;
				case OP_POSTDEC:	printf("--"); break;
				case OP_VECTOR:
					printf("[");
					for(i = 0; i < n->expr.nlist.n; i++) {
						print_ast(n->expr.nlist.nodes[i], idx+1, level, lines);
						if(i < n->expr.nlist.n-1)
							printf(", ");
					}
					printf("]");
					break;
				case OP_VECTORLIST:
					printf("{");
					for(i = 0; i < n->expr.nlist.n; i++) {
						print_ast(n->expr.nlist.nodes[i], idx+1, level, lines);
						if(i < n->expr.nlist.n-1)
							printf(", ");
					}
					printf("}");
					break;
				}
				print_ast(n->expr.right, idx+1, level, lines);
			}
			if(n->expr.inparen)
				printf(")");
			if(!idx)
				printf(";\n");
			break;
		case NT_IF:
			print_pfx(n, level, 0);
			printf("if(");
			print_ast(n->cond.cond, idx+1, level, lines);
			printf(") {\n");
			print_ast(n->cond.ifclause, 0, level+1, lines);
			print_ast(n->cond.elifclauses, 0, level, lines);
			print_pfx(n, level, 0);
			printf("} else {\n");
			print_ast(n->cond.elseclause, 0, level, lines);
			print_pfx(n, level, 1);
			printf("}\n");
			break;
		case NT_ELIF:
			print_pfx(n, level, 0);
			printf("} elif(");
			print_ast(n->cond.cond, 0, level, lines);
			printf(") {\n");
			print_ast(n->cond.ifclause, 0, level+1, lines);
			break;
		case NT_FOR:
			print_pfx(n, level, 0);
			printf("for(");
			print_ast(n->lfor.init, idx+1, level, lines);
			printf("; ");
			print_ast(n->lfor.cond, idx+1, level, lines);
			printf("; ");
			print_ast(n->lfor.inc, idx+1, level, lines);
			printf(") {\n");
			print_ast(n->lfor.stmts, 0, level+1, lines);
			print_pfx(n, level, 1);
			printf("}\n");
			break;
		case NT_WHILE:
			print_pfx(n, level, 0);
			printf("while(");
			print_ast(n->lfor.cond, idx+1, level, lines);
			printf(") {\n");
			print_ast(n->lfor.stmts, 0, level+1, lines);
			print_pfx(n, level, 1);
			printf("}\n");
			break;
		case NT_DOWHILE:
			print_pfx(n, level, 0);
			printf("do {\n");
			print_ast(n->lfor.stmts, 0, level+1, lines);
			print_pfx(n, level, 0);
			printf("} while(\n");
			print_ast(n->lfor.cond, idx+1, level, lines);
			printf(");\n");
			break;
		case NT_FOREACH:
			print_pfx(n, level, 0);
			printf("foreach(");
			print_ast(n->lfe.src, idx+1, level, lines);
			if(n->lfe.dst) {
				printf("; %ls", n->lfe.dst);
			}
			printf(") {\n");
			print_ast(n->lfe.stmts, 0, level+1, lines);
			print_pfx(n, level, 1);
			printf("}\n");
			break;
		case NT_REPEAT:
			print_pfx(n, level, 0);
			printf("repeat(");
			print_ast(n->lfe.src, idx+1, level, lines);
			if(n->lfe.dst) {
				printf("; %ls", n->lfe.dst);
			}
			printf(") {\n");
			print_ast(n->lfe.stmts, 0, level+1, lines);
			print_pfx(n, level, 1);
			printf("}\n");
			break;
		case NT_FUNCTION:
			print_pfx(n, level, 0);
			printf("function %ls(", n->func.id);
			print_ast(n->func.args, idx+1, level, lines);
			printf(") {\n");
			print_ast(n->func.body, 0, level+1, lines);
			print_pfx(n, level, 1);
			printf("}\n");
			break;
		}
	}
}

static void debug_print_ast(const node_t *head, int lines)
{
	print_ast(head, 0, 0, lines);
}

#endif /* USE_DEBUGGER */
