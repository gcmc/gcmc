/* A Bison parser, made by GNU Bison 3.4.1.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2019 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Undocumented macros, especially those whose name start with YY_,
   are private implementation details.  Do not rely on them.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.4.1"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 19 "grammary.y"

#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "utils.h"
#include "grammartypes.h"
#include "builtin.h"
#include "interpreter.h"

#define myassert(x)	do { \
				if(!(x)) { \
					yyfatal("Assertion failed:(%s:%d): %s", __FILE__, __LINE__, #x); \
				} \
			} while(0);
int yylex(void);

static wchar_t *funcname;

static node_t *newnode(int type);
static node_t *alist_new(wchar_t *id, int isref, node_t *e);
static node_t *alist_add(node_t *l, wchar_t *id, int isref, node_t *e);
static node_t *lvar_new(wchar_t *id, node_t *e);
static node_t *cvar_new(wchar_t *id, node_t *e);
static node_t *elist_new(node_t *e);
static node_t *elist_add(node_t *l, node_t *e);
static node_t *vlist_new(node_t *v);
static node_t *vlist_add(node_t *l, node_t *v);
static node_t *vec_new(node_t *e, int allownull);
static node_t *vec_add(node_t *v, node_t *e);
static node_t *node_new_assign(node_t *lv, int op, node_t *rv);
static node_t *node_new(int op, node_t *e);
static node_t *node_add(node_t *tailnode, node_t *newnode);
static node_t *expr_new(node_t *l, node_t *r, int op);
static node_t *expr_new_unary(node_t *id, int op);
static node_t *expr_new_tern(node_t *c, node_t *l, node_t *r, int op);
static node_t *expr_new_call(wchar_t *id, node_t *e);
static node_t *expr_new_id(wchar_t *id);
static node_t *expr_new_idx(node_t *d, node_t *e);
static node_t *expr_new_idxid(node_t *d, wchar_t *id);
static node_t *expr_new_int(int i, int unit);
static node_t *expr_new_flt(double d, int unit);
static node_t *expr_new_str(wchar_t *str);
static node_t *pushtag(node_t *n);
static node_t *poptag(void);
static node_t *gethead(node_t *n);
static void checkfuncname(const wchar_t *fn);
static void check_useless(const node_t *n);
static void check_const_expr(const node_t *n);
static void check_boolean_expr(const node_t *n);
static node_t *debug_add(node_t *n);

dbgfile_t *dbgfiles;
int ndbgfiles;
int nadbgfiles;
int dbgfileidx;

static int *dbgstack;
static int ndbgstack;
static int nadbgstack;

node_t *scripthead;


#line 135 "grammary.c"

# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Use api.header.include to #include this header
   instead of duplicating it here.  */
#ifndef YY_YY_GRAMMARY_H_INCLUDED
# define YY_YY_GRAMMARY_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    FUNCTION = 258,
    FOR = 259,
    FOREACH = 260,
    DO = 261,
    WHILE = 262,
    IF = 263,
    ELIF = 264,
    ELSE = 265,
    BREAK = 266,
    CONTINUE = 267,
    RETURN = 268,
    INCLUDE = 269,
    LOCAL = 270,
    REPEAT = 271,
    CONST = 272,
    TOPEN = 273,
    TCLOSE = 274,
    MM = 275,
    MIL = 276,
    IN = 277,
    DEG = 278,
    RAD = 279,
    IDENT = 280,
    STRING = 281,
    NUMBER = 282,
    FLOAT = 283,
    SUBASSIGN = 284,
    ADDASSIGN = 285,
    MULASSIGN = 286,
    DIVASSIGN = 287,
    MODASSIGN = 288,
    SHLASSIGN = 289,
    SHRASSIGN = 290,
    ADDORASSIGN = 291,
    SUBORASSIGN = 292,
    BORASSIGN = 293,
    BANDASSIGN = 294,
    BXORASSIGN = 295,
    LOR = 296,
    LAND = 297,
    EQ = 298,
    NE = 299,
    LT = 300,
    GT = 301,
    LE = 302,
    GE = 303,
    SHL = 304,
    SHR = 305,
    ADDOR = 306,
    SUBOR = 307,
    INC = 308,
    DEC = 309,
    UPM = 310,
    UID = 311
  };
#endif
/* Tokens.  */
#define FUNCTION 258
#define FOR 259
#define FOREACH 260
#define DO 261
#define WHILE 262
#define IF 263
#define ELIF 264
#define ELSE 265
#define BREAK 266
#define CONTINUE 267
#define RETURN 268
#define INCLUDE 269
#define LOCAL 270
#define REPEAT 271
#define CONST 272
#define TOPEN 273
#define TCLOSE 274
#define MM 275
#define MIL 276
#define IN 277
#define DEG 278
#define RAD 279
#define IDENT 280
#define STRING 281
#define NUMBER 282
#define FLOAT 283
#define SUBASSIGN 284
#define ADDASSIGN 285
#define MULASSIGN 286
#define DIVASSIGN 287
#define MODASSIGN 288
#define SHLASSIGN 289
#define SHRASSIGN 290
#define ADDORASSIGN 291
#define SUBORASSIGN 292
#define BORASSIGN 293
#define BANDASSIGN 294
#define BXORASSIGN 295
#define LOR 296
#define LAND 297
#define EQ 298
#define NE 299
#define LT 300
#define GT 301
#define LE 302
#define GE 303
#define SHL 304
#define SHR 305
#define ADDOR 306
#define SUBOR 307
#define INC 308
#define DEC 309
#define UPM 310
#define UID 311

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 84 "grammary.y"

	wchar_t		*str;
	double		d;
	int		i;
	node_t		*node;

#line 297 "grammary.c"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_GRAMMARY_H_INCLUDED  */



#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && ! defined __ICC && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  87
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   1516

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  77
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  37
/* YYNRULES -- Number of rules.  */
#define YYNRULES  216
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  356

#define YYUNDEFTOK  2
#define YYMAXUTOK   311

/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                                \
  ((unsigned) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    66,     2,     2,     2,    63,    48,     2,
      72,    73,    61,    57,    75,    58,    70,    62,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    43,    74,
       2,    29,     2,    42,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    69,     2,    76,    47,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,    46,     2,    67,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    44,    45,    49,    50,
      51,    52,    53,    54,    55,    56,    59,    60,    64,    65,
      68,    71
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   125,   125,   126,   127,   130,   131,   134,   135,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   154,   155,
     158,   159,   160,   161,   164,   165,   168,   169,   170,   173,
     174,   175,   178,   179,   182,   183,   184,   187,   188,   191,
     192,   195,   196,   199,   202,   203,   206,   209,   212,   215,
     218,   221,   227,   230,   233,   240,   241,   242,   243,   244,
     245,   246,   247,   254,   261,   262,   263,   264,   265,   266,
     267,   268,   276,   277,   278,   279,   280,   281,   282,   283,
     289,   290,   291,   292,   293,   294,   300,   301,   302,   303,
     304,   305,   306,   307,   318,   319,   320,   321,   322,   329,
     337,   338,   339,   340,   341,   342,   343,   344,   345,   346,
     347,   348,   349,   350,   351,   354,   355,   358,   364,   365,
     366,   367,   368,   371,   374,   375,   378,   379,   380,   381,
     384,   385,   397,   398,   399,   400,   401,   402,   403,   404,
     405,   406,   407,   408,   409,   410,   411,   412,   413,   414,
     415,   416,   417,   418,   419,   420,   421,   422,   423,   424,
     425,   426,   427,   428,   429,   430,   431,   432,   433,   434,
     435,   436,   437,   438,   439,   440,   441,   442,   443,   444,
     445,   446,   447,   448,   449,   450,   451,   452,   453,   454,
     455,   456,   457,   458,   459,   462,   463,   464,   465,   466,
     467,   470,   471,   474,   475,   476,   477,   480,   481,   484,
     485,   486,   489,   490,   491,   494,   495
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 1
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "FUNCTION", "FOR", "FOREACH", "DO",
  "WHILE", "IF", "ELIF", "ELSE", "BREAK", "CONTINUE", "RETURN", "INCLUDE",
  "LOCAL", "REPEAT", "CONST", "TOPEN", "TCLOSE", "MM", "MIL", "IN", "DEG",
  "RAD", "IDENT", "STRING", "NUMBER", "FLOAT", "'='", "SUBASSIGN",
  "ADDASSIGN", "MULASSIGN", "DIVASSIGN", "MODASSIGN", "SHLASSIGN",
  "SHRASSIGN", "ADDORASSIGN", "SUBORASSIGN", "BORASSIGN", "BANDASSIGN",
  "BXORASSIGN", "'?'", "':'", "LOR", "LAND", "'|'", "'^'", "'&'", "EQ",
  "NE", "LT", "GT", "LE", "GE", "SHL", "SHR", "'+'", "'-'", "ADDOR",
  "SUBOR", "'*'", "'/'", "'%'", "INC", "DEC", "'!'", "'~'", "UPM", "'['",
  "'.'", "UID", "'('", "')'", "';'", "','", "']'", "$accept", "file",
  "lines", "line", "cvars", "cvar", "optarglist", "arglist", "optv",
  "optref", "locals", "local", "optassgn", "optstmts", "stmt", "optstmt",
  "foreach", "repeat", "for", "while", "do", "function", "boolexpr",
  "compound", "optelif", "elif", "call", "optargs", "args", "expr",
  "optunit", "optcomma", "veclist", "vlist", "vector", "nums", "anynum", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,    61,
     284,   285,   286,   287,   288,   289,   290,   291,   292,   293,
     294,   295,    63,    58,   296,   297,   124,    94,    38,   298,
     299,   300,   301,   302,   303,   304,   305,    43,    45,   306,
     307,    42,    47,    37,   308,   309,    33,   126,   310,    91,
      46,   311,    40,    41,    59,    44,    93
};
# endif

#define YYPACT_NINF -137

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-137)))

#define YYTABLE_NINF -126

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     488,    24,  -137,  -137,  -137,  -137,     5,     8,    20,   234,
      17,    11,  -137,    26,   506,   -41,  -137,   140,   140,  1124,
    1124,  1124,  1124,  1124,  1124,  1090,  1124,  -137,    43,   206,
    -137,    55,    82,   143,   157,   165,    70,   166,  -137,  -137,
    1303,  -137,  -137,  -137,  -137,  -137,   522,  -137,  -137,  -137,
    -137,  -137,  -137,   351,  -137,    22,     6,   -33,  -137,  -137,
      21,     0,  -137,  -137,  -137,  1303,   -42,  1124,  -137,  -137,
    -137,  -137,  -137,  -137,  -137,    83,    83,    83,    83,    83,
      83,  1124,  -137,  1303,     3,  -137,  1216,  -137,  -137,  -137,
    -137,  -137,  -137,   540,  -137,   575,  -137,  1124,  -137,   595,
    -137,   488,  -137,    -1,   630,   648,   664,   682,   717,   737,
     772,   790,   806,   824,   859,   879,   914,  1124,  1124,  1124,
    1124,  1124,  1124,  1124,  1124,  1124,  1124,  1124,  1124,  1124,
    1124,  1124,  1124,  1124,  1124,  1124,  1124,  1124,  -137,  -137,
    1124,    13,  -137,    19,  1303,  -137,  -137,  -137,    63,  -137,
    1124,  -137,  -137,    27,  -137,   932,  -137,    26,  1124,    25,
      -8,    18,  1303,  -137,   948,  -137,  -137,  -137,   416,  -137,
     286,  -137,    58,  -137,    65,   488,    38,  -137,    66,   -29,
      51,  -137,  1303,  -137,  1303,  -137,  1303,  -137,  1303,  -137,
    1303,  -137,  1303,  -137,  1303,  -137,  1303,  -137,  1303,  -137,
    1303,  -137,  1303,  -137,  1303,  -137,  1303,  1261,   130,  1357,
    1381,  1404,  1426,  1446,  1446,   306,   306,   306,   306,   369,
     369,   457,   457,   457,   457,   240,   240,   240,  1168,  -137,
    -137,   226,  -137,    59,  1303,  -137,  -137,  -137,  1303,  -137,
    1303,  -137,  -137,  -137,  -137,   966,  -137,  -137,  -137,    28,
    -137,   132,    29,  -137,  1001,  -137,   250,  -137,    33,  -137,
     256,     7,    40,  1124,  -137,  -137,   488,  -137,  -137,  -137,
    1303,  -137,    67,   488,  -137,    69,  -137,    60,  -137,   488,
    -137,   167,  -137,   488,  -137,   171,  1021,  -137,  1332,    44,
    -137,   265,   146,  -137,   268,  -137,  1124,   150,  -137,  1056,
     207,    40,  -137,  1303,  -137,  -137,  -137,   488,  -137,  -137,
     488,    72,  -137,  -137,  -137,    84,  -137,  -137,  -137,   163,
     224,   227,  -137,   275,  -137,    61,   169,   278,  -137,  -137,
    -137,  -137,  -137,  -137,   488,  -137,  -137,  -137,  1074,  -137,
     488,   229,  -137,    86,   246,  -137,  -137,  -137,   284,  -137,
    -137,  -137,   488,   248,  -137,  -137
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       2,     0,    48,    46,    50,    49,     0,     0,     0,     0,
       0,     0,    47,     0,     0,   159,   162,   195,   195,     0,
       0,     0,     0,     0,     0,     0,     0,    17,     0,     0,
       5,     0,     0,     0,     0,     0,     0,     0,     7,   156,
      43,   164,   163,    52,    51,   106,     0,   112,   111,   114,
     113,   109,   108,     0,    12,     0,     0,     0,    34,    23,
       0,     0,    18,   205,   204,   207,   201,   124,   196,   197,
     198,   199,   200,   160,   161,   130,   131,   165,   166,   132,
     133,   215,   209,   216,     0,   212,     0,     1,     4,     6,
      14,    13,    61,     0,    70,     0,    78,    44,    84,     0,
      92,    41,    97,    24,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   167,   168,
       0,     0,   105,     0,    53,   110,   107,    11,     0,    38,
       0,    37,    15,     0,    22,     0,    16,     0,   202,     0,
       0,     0,   126,   211,     0,   210,   134,    60,     0,    69,
       0,    45,     0,    83,     0,    42,     0,    33,     0,    25,
       0,   182,   169,   185,   172,   183,   170,   187,   174,   188,
     175,   189,   176,   190,   177,   191,   178,   184,   171,   186,
     173,   192,   179,   193,   180,   194,   181,     0,   149,   148,
     153,   154,   152,   142,   143,   145,   144,   147,   146,   150,
     151,   135,   137,   136,   138,   139,   140,   141,     0,   158,
     104,     0,    10,     0,    40,    36,    35,    21,    20,    19,
     208,   206,   203,   123,   129,     0,   214,   213,    59,     0,
      68,     0,     0,    77,     0,    82,     0,    91,     0,    96,
       0,     0,    29,     0,   157,   103,    41,     9,     8,   128,
     127,    58,     0,    41,    67,     0,    76,     0,    81,    41,
      90,     0,    95,    41,    28,     0,     0,    26,   155,     0,
      57,     0,     0,    66,     0,    75,    44,     0,    89,     0,
       0,    29,    31,    30,   102,   115,    56,    41,    63,    65,
      41,     0,    80,    79,    88,     0,    94,    93,    27,    98,
       0,     0,    74,     0,    87,     0,     0,     0,   116,    55,
      54,    64,    62,    73,    41,    86,    85,   122,     0,   101,
      41,     0,   121,     0,     0,    72,    71,   120,     0,   100,
      99,   119,    41,     0,   118,   117
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -137,  -137,   197,   -27,  -137,    46,  -137,  -137,   -96,   -19,
    -137,    97,  -137,  -136,   -94,   -43,  -137,  -137,  -137,  -137,
    -137,  -137,   -98,  -137,  -137,  -137,  -137,  -137,  -137,    -9,
     237,  -137,  -137,  -137,  -137,  -137,    92
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,    28,   175,    30,    61,    62,   178,   179,   287,   180,
      57,    58,   151,   176,    31,   172,    32,    33,    34,    35,
      36,    37,   143,    38,   319,   328,    39,   160,   161,    40,
      73,   159,    41,    66,    42,    84,    85
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      53,   174,    89,   171,   163,    65,    45,   149,   284,    47,
      75,    76,    77,    78,    79,    80,    83,    86,    54,   244,
     230,    49,   154,   147,   -32,    43,   241,    59,   235,   271,
     274,    67,   -32,   158,   280,   150,    56,   144,   229,   257,
     281,   152,   153,    87,   242,   304,   261,   177,   148,    44,
     155,    60,    56,   272,   275,   177,    90,   258,   162,   253,
     267,   295,   335,   305,   232,   243,   255,   259,   290,   286,
     293,   100,    76,   322,   156,   157,   262,    46,   164,   165,
     -39,   -39,    48,    92,   168,   324,   170,   347,   101,    55,
     144,  -125,   231,   245,    50,   182,   184,   186,   188,   190,
     192,   194,   196,   198,   200,   202,   204,   206,   207,   208,
     209,   210,   211,   212,   213,   214,   215,   216,   217,   218,
     219,   220,   221,   222,   223,   224,   225,   226,   227,    91,
     289,   228,   254,   268,   296,   336,   233,   292,   256,   260,
     291,   234,   294,   297,    94,   323,   238,   300,    89,   240,
     273,   312,   140,   141,    93,    83,   277,   325,    96,   348,
      68,    69,    70,    71,    72,   308,    98,   102,   298,   313,
     337,   320,   326,   327,   321,   119,   120,   121,   122,   123,
     124,   125,   126,   127,   128,   129,   130,   131,   132,   133,
     134,   135,   136,   137,   138,   139,   301,    29,   341,   140,
     141,   315,   171,   239,   344,   318,    -3,    88,   316,     1,
       2,     3,     4,     5,     6,    95,   353,     7,     8,     9,
      10,    11,    12,    13,    14,   329,   317,   265,   331,    97,
     345,    15,    16,    17,    18,    51,   270,    99,   103,   299,
     343,   338,   285,   330,   266,   144,   332,   349,   346,   354,
     236,   278,    14,   311,   288,    74,   247,   282,     0,    15,
      16,    17,    18,    19,    20,   350,   306,   355,   279,   309,
      21,    22,    23,    24,   283,    25,   333,   303,    26,   339,
      27,     0,     0,   307,     0,   351,   310,   250,     0,     0,
     144,    19,    20,   334,     0,     0,   340,     0,    21,    22,
      23,    24,   352,    25,   138,   139,    26,     0,    52,   140,
     141,     0,     0,     0,     0,   104,   105,   106,   107,   108,
     109,   110,   111,   112,   113,   114,   115,   116,   117,   144,
     118,   119,   120,   121,   122,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,   137,
     138,   139,   145,     0,     0,   140,   141,     0,     0,   251,
     252,   129,   130,   131,   132,   133,   134,   135,   136,   137,
     138,   139,     0,     0,     0,   140,   141,     0,     0,     0,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,     0,   118,   119,   120,   121,   122,
     123,   124,   125,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,   137,   138,   139,   248,     0,     0,
     140,   141,     0,     0,     0,   146,   131,   132,   133,   134,
     135,   136,   137,   138,   139,     0,     0,     0,   140,   141,
       0,     0,     0,     0,     0,   104,   105,   106,   107,   108,
     109,   110,   111,   112,   113,   114,   115,   116,   117,     0,
     118,   119,   120,   121,   122,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,   137,
     138,   139,     0,     0,     0,   140,   141,     0,     0,     0,
     249,     1,     2,     3,     4,     5,     6,     0,     0,     7,
       8,     9,    10,    11,    12,    13,    14,    63,     0,     0,
       0,     0,     0,    15,    16,    17,    18,     0,   135,   136,
     137,   138,   139,   142,    14,    64,   140,   141,     0,     0,
       0,    15,    16,    17,    18,     0,     0,     0,     0,     0,
      14,   167,     0,     0,     0,    19,    20,    15,    16,    17,
      18,     0,    21,    22,    23,    24,     0,    25,    14,     0,
      26,     0,    27,    19,    20,    15,    16,    17,    18,     0,
      21,    22,    23,    24,     0,    25,   169,     0,    26,    19,
      20,     0,     0,     0,     0,     0,    21,    22,    23,    24,
       0,    25,     0,    14,    26,     0,   173,    19,    20,     0,
      15,    16,    17,    18,    21,    22,    23,    24,     0,    25,
       0,     0,    26,    14,     0,     0,     0,     0,     0,     0,
      15,    16,    17,    18,     0,     0,     0,     0,     0,     0,
       0,   181,    19,    20,     0,     0,     0,     0,     0,    21,
      22,    23,    24,     0,    25,     0,     0,    26,    14,   183,
       0,     0,    19,    20,     0,    15,    16,    17,    18,    21,
      22,    23,    24,     0,    25,   185,    14,    26,     0,     0,
       0,     0,     0,    15,    16,    17,    18,     0,     0,     0,
       0,     0,    14,   187,     0,     0,     0,    19,    20,    15,
      16,    17,    18,     0,    21,    22,    23,    24,     0,    25,
      14,     0,    26,     0,     0,    19,    20,    15,    16,    17,
      18,     0,    21,    22,    23,    24,     0,    25,   189,     0,
      26,    19,    20,     0,     0,     0,     0,     0,    21,    22,
      23,    24,     0,    25,     0,    14,    26,     0,   191,    19,
      20,     0,    15,    16,    17,    18,    21,    22,    23,    24,
       0,    25,     0,     0,    26,    14,     0,     0,     0,     0,
       0,     0,    15,    16,    17,    18,     0,     0,     0,     0,
       0,     0,     0,   193,    19,    20,     0,     0,     0,     0,
       0,    21,    22,    23,    24,     0,    25,     0,     0,    26,
      14,   195,     0,     0,    19,    20,     0,    15,    16,    17,
      18,    21,    22,    23,    24,     0,    25,   197,    14,    26,
       0,     0,     0,     0,     0,    15,    16,    17,    18,     0,
       0,     0,     0,     0,    14,   199,     0,     0,     0,    19,
      20,    15,    16,    17,    18,     0,    21,    22,    23,    24,
       0,    25,    14,     0,    26,     0,     0,    19,    20,    15,
      16,    17,    18,     0,    21,    22,    23,    24,     0,    25,
     201,     0,    26,    19,    20,     0,     0,     0,     0,     0,
      21,    22,    23,    24,     0,    25,     0,    14,    26,     0,
     203,    19,    20,     0,    15,    16,    17,    18,    21,    22,
      23,    24,     0,    25,     0,     0,    26,    14,     0,     0,
       0,     0,     0,     0,    15,    16,    17,    18,     0,     0,
       0,     0,     0,     0,     0,   205,    19,    20,     0,     0,
       0,     0,     0,    21,    22,    23,    24,     0,    25,     0,
       0,    26,    14,   237,     0,     0,    19,    20,     0,    15,
      16,    17,    18,    21,    22,    23,    24,     0,    25,   246,
      14,    26,     0,     0,     0,     0,     0,    15,    16,    17,
      18,     0,     0,     0,     0,     0,    14,   269,     0,     0,
       0,    19,    20,    15,    16,    17,    18,     0,    21,    22,
      23,    24,     0,    25,    14,     0,    26,     0,     0,    19,
      20,    15,    16,    17,    18,     0,    21,    22,    23,    24,
       0,    25,   276,     0,    26,    19,    81,     0,     0,     0,
       0,     0,    21,    22,    23,    24,     0,    25,     0,    14,
      26,     0,   302,    19,    20,     0,    15,    16,    17,    18,
      21,    22,    23,    24,     0,    25,     0,     0,    26,    14,
       0,     0,     0,     0,     0,     0,    15,    16,    17,    18,
       0,     0,     0,     0,     0,     0,     0,   314,    19,    20,
       0,     0,     0,     0,     0,    21,    22,    23,    24,     0,
      25,     0,     0,    26,    14,   342,     0,     0,    19,    20,
       0,    15,    16,    17,    18,    21,    22,    23,    24,     0,
      25,     0,    14,    26,     0,     0,     0,     0,     0,    15,
      16,    17,    18,     0,     0,     0,     0,     0,    14,     0,
       0,     0,     0,    19,    20,    15,    16,    17,    18,     0,
      21,    22,    23,    24,     0,    25,     0,     0,    26,     0,
       0,    19,    20,     0,     0,     0,     0,     0,    21,    22,
      23,    24,    14,    25,     0,     0,    26,    19,    81,    15,
      16,    17,    18,     0,    21,    22,    23,    24,     0,    25,
       0,     0,    26,     0,     0,     0,    82,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    19,    20,     0,     0,     0,     0,     0,    21,    22,
      23,    24,     0,    25,     0,     0,    26,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,   115,   116,
     117,     0,   118,   119,   120,   121,   122,   123,   124,   125,
     126,   127,   128,   129,   130,   131,   132,   133,   134,   135,
     136,   137,   138,   139,     0,     0,     0,   140,   141,     0,
       0,     0,     0,     0,   264,   104,   105,   106,   107,   108,
     109,   110,   111,   112,   113,   114,   115,   116,   117,     0,
     118,   119,   120,   121,   122,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,   137,
     138,   139,     0,     0,     0,   140,   141,     0,     0,   166,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   263,   118,   119,   120,   121,   122,
     123,   124,   125,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,   137,   138,   139,     0,     0,     0,
     140,   141,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,     0,   118,   119,   120,
     121,   122,   123,   124,   125,   126,   127,   128,   129,   130,
     131,   132,   133,   134,   135,   136,   137,   138,   139,     0,
       0,     0,   140,   141,   117,     0,   118,   119,   120,   121,
     122,   123,   124,   125,   126,   127,   128,   129,   130,   131,
     132,   133,   134,   135,   136,   137,   138,   139,     0,     0,
       0,   140,   141,   120,   121,   122,   123,   124,   125,   126,
     127,   128,   129,   130,   131,   132,   133,   134,   135,   136,
     137,   138,   139,     0,     0,     0,   140,   141,   121,   122,
     123,   124,   125,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,   137,   138,   139,     0,     0,     0,
     140,   141,   122,   123,   124,   125,   126,   127,   128,   129,
     130,   131,   132,   133,   134,   135,   136,   137,   138,   139,
       0,     0,     0,   140,   141,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,   137,
     138,   139,     0,     0,     0,   140,   141,   125,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,   137,
     138,   139,     0,     0,     0,   140,   141
};

static const yytype_int16 yycheck[] =
{
       9,    99,    29,    97,     1,    14,     1,     1,     1,     1,
      19,    20,    21,    22,    23,    24,    25,    26,     1,     1,
       1,     1,     1,     1,    25,     1,     1,     1,     1,     1,
       1,    72,    25,    75,     1,    29,    25,    46,    25,     1,
       7,    74,    75,     0,    19,     1,    75,    48,    26,    25,
      29,    25,    25,    25,    25,    48,     1,    19,    67,     1,
       1,     1,     1,    19,     1,    73,     1,     1,     1,    29,
       1,     1,    81,     1,    74,    75,    25,    72,    75,    76,
      74,    75,    74,     1,    93,     1,    95,     1,    18,    72,
      99,    73,    73,    75,    74,   104,   105,   106,   107,   108,
     109,   110,   111,   112,   113,   114,   115,   116,   117,   118,
     119,   120,   121,   122,   123,   124,   125,   126,   127,   128,
     129,   130,   131,   132,   133,   134,   135,   136,   137,    74,
     266,   140,    74,    74,    74,    74,    73,   273,    73,    73,
      73,   150,    73,   279,     1,    73,   155,   283,   175,   158,
      18,     1,    69,    70,    72,   164,   254,    73,     1,    73,
      20,    21,    22,    23,    24,    19,     1,     1,     1,    19,
       1,   307,     9,    10,   310,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    25,     0,   334,    69,
      70,   299,   296,   157,   340,   301,     0,     1,     1,     3,
       4,     5,     6,     7,     8,    72,   352,    11,    12,    13,
      14,    15,    16,    17,    18,     1,    19,     1,     1,    72,
       1,    25,    26,    27,    28,     1,   245,    72,    72,    72,
     338,    72,   261,    19,    18,   254,    19,     1,    19,     1,
     153,     1,    18,   296,   263,    18,   164,     1,    -1,    25,
      26,    27,    28,    57,    58,    19,     1,    19,    18,     1,
      64,    65,    66,    67,    18,    69,     1,   286,    72,     1,
      74,    -1,    -1,    18,    -1,     1,    18,     1,    -1,    -1,
     299,    57,    58,    18,    -1,    -1,    18,    -1,    64,    65,
      66,    67,    18,    69,    64,    65,    72,    -1,    74,    69,
      70,    -1,    -1,    -1,    -1,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,   338,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,     1,    -1,    -1,    69,    70,    -1,    -1,    73,
      74,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    -1,    -1,    -1,    69,    70,    -1,    -1,    -1,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    -1,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
      59,    60,    61,    62,    63,    64,    65,     1,    -1,    -1,
      69,    70,    -1,    -1,    -1,    74,    57,    58,    59,    60,
      61,    62,    63,    64,    65,    -1,    -1,    -1,    69,    70,
      -1,    -1,    -1,    -1,    -1,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    -1,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    -1,    -1,    -1,    69,    70,    -1,    -1,    -1,
      74,     3,     4,     5,     6,     7,     8,    -1,    -1,    11,
      12,    13,    14,    15,    16,    17,    18,     1,    -1,    -1,
      -1,    -1,    -1,    25,    26,    27,    28,    -1,    61,    62,
      63,    64,    65,     1,    18,    19,    69,    70,    -1,    -1,
      -1,    25,    26,    27,    28,    -1,    -1,    -1,    -1,    -1,
      18,     1,    -1,    -1,    -1,    57,    58,    25,    26,    27,
      28,    -1,    64,    65,    66,    67,    -1,    69,    18,    -1,
      72,    -1,    74,    57,    58,    25,    26,    27,    28,    -1,
      64,    65,    66,    67,    -1,    69,     1,    -1,    72,    57,
      58,    -1,    -1,    -1,    -1,    -1,    64,    65,    66,    67,
      -1,    69,    -1,    18,    72,    -1,     1,    57,    58,    -1,
      25,    26,    27,    28,    64,    65,    66,    67,    -1,    69,
      -1,    -1,    72,    18,    -1,    -1,    -1,    -1,    -1,    -1,
      25,    26,    27,    28,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,     1,    57,    58,    -1,    -1,    -1,    -1,    -1,    64,
      65,    66,    67,    -1,    69,    -1,    -1,    72,    18,     1,
      -1,    -1,    57,    58,    -1,    25,    26,    27,    28,    64,
      65,    66,    67,    -1,    69,     1,    18,    72,    -1,    -1,
      -1,    -1,    -1,    25,    26,    27,    28,    -1,    -1,    -1,
      -1,    -1,    18,     1,    -1,    -1,    -1,    57,    58,    25,
      26,    27,    28,    -1,    64,    65,    66,    67,    -1,    69,
      18,    -1,    72,    -1,    -1,    57,    58,    25,    26,    27,
      28,    -1,    64,    65,    66,    67,    -1,    69,     1,    -1,
      72,    57,    58,    -1,    -1,    -1,    -1,    -1,    64,    65,
      66,    67,    -1,    69,    -1,    18,    72,    -1,     1,    57,
      58,    -1,    25,    26,    27,    28,    64,    65,    66,    67,
      -1,    69,    -1,    -1,    72,    18,    -1,    -1,    -1,    -1,
      -1,    -1,    25,    26,    27,    28,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,     1,    57,    58,    -1,    -1,    -1,    -1,
      -1,    64,    65,    66,    67,    -1,    69,    -1,    -1,    72,
      18,     1,    -1,    -1,    57,    58,    -1,    25,    26,    27,
      28,    64,    65,    66,    67,    -1,    69,     1,    18,    72,
      -1,    -1,    -1,    -1,    -1,    25,    26,    27,    28,    -1,
      -1,    -1,    -1,    -1,    18,     1,    -1,    -1,    -1,    57,
      58,    25,    26,    27,    28,    -1,    64,    65,    66,    67,
      -1,    69,    18,    -1,    72,    -1,    -1,    57,    58,    25,
      26,    27,    28,    -1,    64,    65,    66,    67,    -1,    69,
       1,    -1,    72,    57,    58,    -1,    -1,    -1,    -1,    -1,
      64,    65,    66,    67,    -1,    69,    -1,    18,    72,    -1,
       1,    57,    58,    -1,    25,    26,    27,    28,    64,    65,
      66,    67,    -1,    69,    -1,    -1,    72,    18,    -1,    -1,
      -1,    -1,    -1,    -1,    25,    26,    27,    28,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,     1,    57,    58,    -1,    -1,
      -1,    -1,    -1,    64,    65,    66,    67,    -1,    69,    -1,
      -1,    72,    18,     1,    -1,    -1,    57,    58,    -1,    25,
      26,    27,    28,    64,    65,    66,    67,    -1,    69,     1,
      18,    72,    -1,    -1,    -1,    -1,    -1,    25,    26,    27,
      28,    -1,    -1,    -1,    -1,    -1,    18,     1,    -1,    -1,
      -1,    57,    58,    25,    26,    27,    28,    -1,    64,    65,
      66,    67,    -1,    69,    18,    -1,    72,    -1,    -1,    57,
      58,    25,    26,    27,    28,    -1,    64,    65,    66,    67,
      -1,    69,     1,    -1,    72,    57,    58,    -1,    -1,    -1,
      -1,    -1,    64,    65,    66,    67,    -1,    69,    -1,    18,
      72,    -1,     1,    57,    58,    -1,    25,    26,    27,    28,
      64,    65,    66,    67,    -1,    69,    -1,    -1,    72,    18,
      -1,    -1,    -1,    -1,    -1,    -1,    25,    26,    27,    28,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,     1,    57,    58,
      -1,    -1,    -1,    -1,    -1,    64,    65,    66,    67,    -1,
      69,    -1,    -1,    72,    18,     1,    -1,    -1,    57,    58,
      -1,    25,    26,    27,    28,    64,    65,    66,    67,    -1,
      69,    -1,    18,    72,    -1,    -1,    -1,    -1,    -1,    25,
      26,    27,    28,    -1,    -1,    -1,    -1,    -1,    18,    -1,
      -1,    -1,    -1,    57,    58,    25,    26,    27,    28,    -1,
      64,    65,    66,    67,    -1,    69,    -1,    -1,    72,    -1,
      -1,    57,    58,    -1,    -1,    -1,    -1,    -1,    64,    65,
      66,    67,    18,    69,    -1,    -1,    72,    57,    58,    25,
      26,    27,    28,    -1,    64,    65,    66,    67,    -1,    69,
      -1,    -1,    72,    -1,    -1,    -1,    76,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    57,    58,    -1,    -1,    -1,    -1,    -1,    64,    65,
      66,    67,    -1,    69,    -1,    -1,    72,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    40,    41,
      42,    -1,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    -1,    -1,    -1,    69,    70,    -1,
      -1,    -1,    -1,    -1,    76,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    -1,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    -1,    -1,    -1,    69,    70,    -1,    -1,    73,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
      59,    60,    61,    62,    63,    64,    65,    -1,    -1,    -1,
      69,    70,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    40,    41,    42,    -1,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    59,    60,    61,    62,    63,    64,    65,    -1,
      -1,    -1,    69,    70,    42,    -1,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    63,    64,    65,    -1,    -1,
      -1,    69,    70,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    57,    58,    59,    60,    61,    62,
      63,    64,    65,    -1,    -1,    -1,    69,    70,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
      59,    60,    61,    62,    63,    64,    65,    -1,    -1,    -1,
      69,    70,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      -1,    -1,    -1,    69,    70,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    -1,    -1,    -1,    69,    70,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    -1,    -1,    -1,    69,    70
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     3,     4,     5,     6,     7,     8,    11,    12,    13,
      14,    15,    16,    17,    18,    25,    26,    27,    28,    57,
      58,    64,    65,    66,    67,    69,    72,    74,    78,    79,
      80,    91,    93,    94,    95,    96,    97,    98,   100,   103,
     106,   109,   111,     1,    25,     1,    72,     1,    74,     1,
      74,     1,    74,   106,     1,    72,    25,    87,    88,     1,
      25,    81,    82,     1,    19,   106,   110,    72,    20,    21,
      22,    23,    24,   107,   107,   106,   106,   106,   106,   106,
     106,    58,    76,   106,   112,   113,   106,     0,     1,    80,
       1,    74,     1,    72,     1,    72,     1,    72,     1,    72,
       1,    18,     1,    72,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      69,    70,     1,    99,   106,     1,    74,     1,    26,     1,
      29,    89,    74,    75,     1,    29,    74,    75,    75,   108,
     104,   105,   106,     1,    75,    76,    73,     1,   106,     1,
     106,    91,    92,     1,    99,    79,    90,    48,    83,    84,
      86,     1,   106,     1,   106,     1,   106,     1,   106,     1,
     106,     1,   106,     1,   106,     1,   106,     1,   106,     1,
     106,     1,   106,     1,   106,     1,   106,   106,   106,   106,
     106,   106,   106,   106,   106,   106,   106,   106,   106,   106,
     106,   106,   106,   106,   106,   106,   106,   106,   106,    25,
       1,    73,     1,    73,   106,     1,    88,     1,   106,    82,
     106,     1,    19,    73,     1,    75,     1,   113,     1,    74,
       1,    73,    74,     1,    74,     1,    73,     1,    19,     1,
      73,    75,    25,    43,    76,     1,    18,     1,    74,     1,
     106,     1,    25,    18,     1,    25,     1,    99,     1,    18,
       1,     7,     1,    18,     1,    86,    29,    85,   106,    90,
       1,    73,    90,     1,    73,     1,    74,    90,     1,    72,
      90,    25,     1,   106,     1,    19,     1,    18,    19,     1,
      18,    92,     1,    19,     1,    99,     1,    19,    85,   101,
      90,    90,     1,    73,     1,    73,     9,    10,   102,     1,
      19,     1,    19,     1,    18,     1,    74,     1,    72,     1,
      18,    90,     1,    99,    90,     1,    19,     1,    73,     1,
      19,     1,    18,    90,     1,    19
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    77,    78,    78,    78,    79,    79,    80,    80,    80,
      80,    80,    80,    80,    80,    80,    80,    80,    81,    81,
      82,    82,    82,    82,    83,    83,    84,    84,    84,    85,
      85,    85,    86,    86,    87,    87,    87,    88,    88,    89,
      89,    90,    90,    91,    92,    92,    93,    94,    95,    96,
      97,    98,    98,    99,   100,   100,   100,   100,   100,   100,
     100,   100,   100,   100,   100,   100,   100,   100,   100,   100,
     100,   100,   100,   100,   100,   100,   100,   100,   100,   100,
     100,   100,   100,   100,   100,   100,   100,   100,   100,   100,
     100,   100,   100,   100,   100,   100,   100,   100,   100,   100,
     100,   100,   100,   100,   100,   100,   100,   100,   100,   100,
     100,   100,   100,   100,   100,   101,   101,   102,   102,   102,
     102,   102,   102,   103,   104,   104,   105,   105,   105,   105,
     106,   106,   106,   106,   106,   106,   106,   106,   106,   106,
     106,   106,   106,   106,   106,   106,   106,   106,   106,   106,
     106,   106,   106,   106,   106,   106,   106,   106,   106,   106,
     106,   106,   106,   106,   106,   106,   106,   106,   106,   106,
     106,   106,   106,   106,   106,   106,   106,   106,   106,   106,
     106,   106,   106,   106,   106,   106,   106,   106,   106,   106,
     106,   106,   106,   106,   106,   107,   107,   107,   107,   107,
     107,   108,   108,   109,   109,   109,   109,   110,   110,   111,
     111,   111,   112,   112,   112,   113,   113
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     0,     1,     2,     1,     2,     1,     5,     5,
       4,     3,     2,     2,     2,     3,     3,     1,     1,     3,
       3,     3,     2,     1,     0,     1,     3,     5,     3,     0,
       2,     2,     0,     1,     1,     3,     3,     2,     2,     0,
       2,     0,     1,     1,     0,     1,     1,     1,     1,     1,
       1,     2,     2,     1,     9,     9,     7,     6,     5,     4,
       3,     2,     9,     7,     9,     7,     6,     5,     4,     3,
       2,    11,    11,     9,     8,     6,     5,     4,     2,     7,
       7,     5,     4,     3,     2,     9,     9,     8,     7,     6,
       5,     4,     2,     7,     7,     5,     4,     2,     8,    12,
      12,    10,     7,     5,     4,     3,     2,     3,     2,     2,
       3,     2,     2,     2,     2,     0,     2,     7,     7,     5,
       4,     3,     2,     4,     0,     1,     1,     3,     3,     2,
       2,     2,     2,     2,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     5,     1,     4,     3,     1,
       2,     2,     1,     1,     1,     2,     2,     2,     2,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     0,     1,     1,     1,     1,
       1,     0,     1,     4,     2,     2,     4,     1,     3,     2,
       3,     3,     1,     3,     3,     1,     1
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyoutput = yyo;
  YYUSE (yyoutput);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyo, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyo, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyo, yytype, yyvaluep);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &yyvsp[(yyi + 1) - (yynrhs)]
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            else
              goto append;

          append:
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return (YYSIZE_T) (yystpcpy (yyres, yystr) - yyres);
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM)
                    yysize = yysize1;
                  else
                    return 2;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
    default: /* Avoid compiler warnings. */
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM)
      yysize = yysize1;
    else
      return 2;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yynewstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  *yyssp = (yytype_int16) yystate;

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    goto yyexhaustedlab;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = (YYSIZE_T) (yyssp - yyss + 1);

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
# undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 2:
#line 125 "grammary.y"
    { ; }
#line 1880 "grammary.c"
    break;

  case 3:
#line 126 "grammary.y"
    { scripthead = gethead((yyvsp[0].node)); }
#line 1886 "grammary.c"
    break;

  case 4:
#line 127 "grammary.y"
    { yyerror("Syntax error"); }
#line 1892 "grammary.c"
    break;

  case 5:
#line 130 "grammary.y"
    { (yyval.node) = (yyvsp[0].node); }
#line 1898 "grammary.c"
    break;

  case 6:
#line 131 "grammary.y"
    { (yyval.node) = node_add((yyvsp[-1].node), (yyvsp[0].node)); }
#line 1904 "grammary.c"
    break;

  case 7:
#line 134 "grammary.y"
    { (yyval.node) = (yyvsp[0].node); }
#line 1910 "grammary.c"
    break;

  case 8:
#line 135 "grammary.y"
    {
			/* Include files is a language construct */
			/* We should get here without a lookahead symbol */
			assert(yychar == YYEMPTY);
			handle_include((yyvsp[-2].str));	/* Setup flex */
			free((yyvsp[-2].str));
			(yyval.node) = NULL;
		}
#line 1923 "grammary.c"
    break;

  case 9:
#line 143 "grammary.y"
    { (yyval.node) = NULL; yyerror("';' expected"); }
#line 1929 "grammary.c"
    break;

  case 10:
#line 144 "grammary.y"
    { (yyval.node) = NULL; yyerror("')' expected"); }
#line 1935 "grammary.c"
    break;

  case 11:
#line 145 "grammary.y"
    { (yyval.node) = NULL; yyerror("String with include filename expected"); }
#line 1941 "grammary.c"
    break;

  case 12:
#line 146 "grammary.y"
    { (yyval.node) = NULL; yyerror("'(' expected"); }
#line 1947 "grammary.c"
    break;

  case 13:
#line 147 "grammary.y"
    { (yyval.node) = debug_add((yyvsp[-1].node)); }
#line 1953 "grammary.c"
    break;

  case 14:
#line 148 "grammary.y"
    { (yyval.node) = NULL; yyerror("';' expected"); }
#line 1959 "grammary.c"
    break;

  case 15:
#line 149 "grammary.y"
    { (yyval.node) = debug_add((yyvsp[-1].node)); }
#line 1965 "grammary.c"
    break;

  case 16:
#line 150 "grammary.y"
    { (yyval.node) = debug_add((yyvsp[-1].node)); }
#line 1971 "grammary.c"
    break;

  case 17:
#line 151 "grammary.y"
    { (yyval.node) = NULL; }
#line 1977 "grammary.c"
    break;

  case 18:
#line 154 "grammary.y"
    { (yyval.node) = (yyvsp[0].node); }
#line 1983 "grammary.c"
    break;

  case 19:
#line 155 "grammary.y"
    { (yyval.node) = node_add((yyvsp[-2].node), (yyvsp[0].node)); }
#line 1989 "grammary.c"
    break;

  case 20:
#line 158 "grammary.y"
    { (yyval.node) = cvar_new((yyvsp[-2].str), (yyvsp[0].node)); }
#line 1995 "grammary.c"
    break;

  case 21:
#line 159 "grammary.y"
    { (yyval.node) = NULL; yyerror("Expression expected"); }
#line 2001 "grammary.c"
    break;

  case 22:
#line 160 "grammary.y"
    { (yyval.node) = NULL; yyerror("'=' expected"); }
#line 2007 "grammary.c"
    break;

  case 23:
#line 161 "grammary.y"
    { (yyval.node) = NULL; yyerror("Identifier expected"); }
#line 2013 "grammary.c"
    break;

  case 24:
#line 164 "grammary.y"
    { (yyval.node) = NULL; }
#line 2019 "grammary.c"
    break;

  case 25:
#line 165 "grammary.y"
    { (yyval.node) = (yyvsp[0].node); }
#line 2025 "grammary.c"
    break;

  case 26:
#line 168 "grammary.y"
    { (yyval.node) = alist_new((yyvsp[-1].str), (yyvsp[-2].i), (yyvsp[0].node)); }
#line 2031 "grammary.c"
    break;

  case 27:
#line 169 "grammary.y"
    { (yyval.node) = alist_add((yyvsp[-4].node), (yyvsp[-1].str), (yyvsp[-2].i), (yyvsp[0].node)); }
#line 2037 "grammary.c"
    break;

  case 28:
#line 170 "grammary.y"
    { (yyval.node) = NULL; yyerror("Identifier expected"); }
#line 2043 "grammary.c"
    break;

  case 29:
#line 173 "grammary.y"
    { (yyval.node) = NULL; }
#line 2049 "grammary.c"
    break;

  case 30:
#line 174 "grammary.y"
    { (yyval.node) = (yyvsp[0].node); }
#line 2055 "grammary.c"
    break;

  case 31:
#line 175 "grammary.y"
    { (yyval.node) = NULL; yyerror("Constant expression expected"); }
#line 2061 "grammary.c"
    break;

  case 32:
#line 178 "grammary.y"
    { (yyval.i) = 0; }
#line 2067 "grammary.c"
    break;

  case 33:
#line 179 "grammary.y"
    { (yyval.i) = 1; }
#line 2073 "grammary.c"
    break;

  case 34:
#line 182 "grammary.y"
    { (yyval.node) = (yyvsp[0].node); }
#line 2079 "grammary.c"
    break;

  case 35:
#line 183 "grammary.y"
    { (yyval.node) = node_add((yyvsp[-2].node), (yyvsp[0].node)); }
#line 2085 "grammary.c"
    break;

  case 36:
#line 184 "grammary.y"
    { (yyval.node) = NULL; yyerror("Identifier expected"); }
#line 2091 "grammary.c"
    break;

  case 37:
#line 187 "grammary.y"
    { (yyval.node) = lvar_new((yyvsp[-1].str), (yyvsp[0].node)); }
#line 2097 "grammary.c"
    break;

  case 38:
#line 188 "grammary.y"
    { (yyval.node) = NULL; yyerror("Assignment expected"); }
#line 2103 "grammary.c"
    break;

  case 39:
#line 191 "grammary.y"
    { (yyval.node) = NULL; }
#line 2109 "grammary.c"
    break;

  case 40:
#line 192 "grammary.y"
    { (yyval.node) = (yyvsp[0].node); }
#line 2115 "grammary.c"
    break;

  case 41:
#line 195 "grammary.y"
    { (yyval.node) = NULL; }
#line 2121 "grammary.c"
    break;

  case 42:
#line 196 "grammary.y"
    { (yyval.node) = gethead((yyvsp[0].node)); }
#line 2127 "grammary.c"
    break;

  case 43:
#line 199 "grammary.y"
    { (yyval.node) = (yyvsp[0].node); check_const_expr((yyvsp[0].node)); }
#line 2133 "grammary.c"
    break;

  case 44:
#line 202 "grammary.y"
    { (yyval.node) = NULL; }
#line 2139 "grammary.c"
    break;

  case 45:
#line 203 "grammary.y"
    { (yyval.node) = (yyvsp[0].node); }
#line 2145 "grammary.c"
    break;

  case 46:
#line 206 "grammary.y"
    { pushtag(newnode(NT_FOREACH)); }
#line 2151 "grammary.c"
    break;

  case 47:
#line 209 "grammary.y"
    { pushtag(newnode(NT_REPEAT)); }
#line 2157 "grammary.c"
    break;

  case 48:
#line 212 "grammary.y"
    { pushtag(newnode(NT_FOR)); }
#line 2163 "grammary.c"
    break;

  case 49:
#line 215 "grammary.y"
    { pushtag(newnode(NT_WHILE)); }
#line 2169 "grammary.c"
    break;

  case 50:
#line 218 "grammary.y"
    { pushtag(newnode(NT_DOWHILE)); }
#line 2175 "grammary.c"
    break;

  case 51:
#line 221 "grammary.y"
    {
			if(funcname)
				yyerror("Function in functions not supported");
			checkfuncname((yyvsp[0].str));
			(yyval.str) = funcname = (yyvsp[0].str);
		}
#line 2186 "grammary.c"
    break;

  case 52:
#line 227 "grammary.y"
    { (yyval.str) = NULL; yyerror("Function name expected"); }
#line 2192 "grammary.c"
    break;

  case 53:
#line 230 "grammary.y"
    { (yyval.node) = (yyvsp[0].node); check_boolean_expr((yyval.node)); }
#line 2198 "grammary.c"
    break;

  case 54:
#line 233 "grammary.y"
    {
			(yyval.node) = poptag();
			(yyval.node)->lfe.src = (yyvsp[-6].node);
			(yyval.node)->lfe.dst = (yyvsp[-4].str);
			(yyval.node)->lfe.stmts = (yyvsp[-1].node);
			(yyval.node) = debug_add((yyval.node));
		}
#line 2210 "grammary.c"
    break;

  case 55:
#line 240 "grammary.y"
    { (yyval.node) = NULL; yyerror("'}' expected"); }
#line 2216 "grammary.c"
    break;

  case 56:
#line 241 "grammary.y"
    { (yyval.node) = NULL; yyerror("'{' expected"); }
#line 2222 "grammary.c"
    break;

  case 57:
#line 242 "grammary.y"
    { (yyval.node) = NULL; yyerror("')' expected"); }
#line 2228 "grammary.c"
    break;

  case 58:
#line 243 "grammary.y"
    { (yyval.node) = NULL; yyerror("Identifier expected"); }
#line 2234 "grammary.c"
    break;

  case 59:
#line 244 "grammary.y"
    { (yyval.node) = NULL; yyerror("';' expected"); }
#line 2240 "grammary.c"
    break;

  case 60:
#line 245 "grammary.y"
    { (yyval.node) = NULL; yyerror("Vectorlist expression expected"); }
#line 2246 "grammary.c"
    break;

  case 61:
#line 246 "grammary.y"
    { (yyval.node) = NULL; yyerror("'(' expected"); }
#line 2252 "grammary.c"
    break;

  case 62:
#line 247 "grammary.y"
    {
			(yyval.node) = poptag();
			(yyval.node)->lfe.src = (yyvsp[-6].node);
			(yyval.node)->lfe.dst = (yyvsp[-4].str);
			(yyval.node)->lfe.stmts = (yyvsp[-1].node);
			(yyval.node) = debug_add((yyval.node));
		}
#line 2264 "grammary.c"
    break;

  case 63:
#line 254 "grammary.y"
    {
			(yyval.node) = poptag();
			(yyval.node)->lfe.src = (yyvsp[-4].node);
			(yyval.node)->lfe.dst = NULL;
			(yyval.node)->lfe.stmts = (yyvsp[-1].node);
			(yyval.node) = debug_add((yyval.node));
		}
#line 2276 "grammary.c"
    break;

  case 64:
#line 261 "grammary.y"
    { (yyval.node) = NULL; yyerror("'}' expected"); }
#line 2282 "grammary.c"
    break;

  case 65:
#line 262 "grammary.y"
    { (yyval.node) = NULL; yyerror("'{' expected"); }
#line 2288 "grammary.c"
    break;

  case 66:
#line 263 "grammary.y"
    { (yyval.node) = NULL; yyerror("')' expected"); }
#line 2294 "grammary.c"
    break;

  case 67:
#line 264 "grammary.y"
    { (yyval.node) = NULL; yyerror("Identifier expected"); }
#line 2300 "grammary.c"
    break;

  case 68:
#line 265 "grammary.y"
    { (yyval.node) = NULL; yyerror("';' or ')' expected"); }
#line 2306 "grammary.c"
    break;

  case 69:
#line 266 "grammary.y"
    { (yyval.node) = NULL; yyerror("Integer expression expected"); }
#line 2312 "grammary.c"
    break;

  case 70:
#line 267 "grammary.y"
    { (yyval.node) = NULL; yyerror("'(' expected"); }
#line 2318 "grammary.c"
    break;

  case 71:
#line 268 "grammary.y"
    {
			(yyval.node) = poptag();
			(yyval.node)->lfor.init = (yyvsp[-8].node);
			(yyval.node)->lfor.cond = (yyvsp[-6].node);
			(yyval.node)->lfor.inc = (yyvsp[-4].node);
			(yyval.node)->lfor.stmts = (yyvsp[-1].node);
			(yyval.node) = debug_add((yyval.node));
		}
#line 2331 "grammary.c"
    break;

  case 72:
#line 276 "grammary.y"
    { (yyval.node) = NULL; yyerror("'}' expected"); }
#line 2337 "grammary.c"
    break;

  case 73:
#line 277 "grammary.y"
    { (yyval.node) = NULL; yyerror("'{' expected"); }
#line 2343 "grammary.c"
    break;

  case 74:
#line 278 "grammary.y"
    { (yyval.node) = NULL; yyerror("')' expected"); }
#line 2349 "grammary.c"
    break;

  case 75:
#line 279 "grammary.y"
    { (yyval.node) = NULL; yyerror("';' expected"); }
#line 2355 "grammary.c"
    break;

  case 76:
#line 280 "grammary.y"
    { (yyval.node) = NULL; yyerror("Conditional expression expected"); }
#line 2361 "grammary.c"
    break;

  case 77:
#line 281 "grammary.y"
    { (yyval.node) = NULL; yyerror("';' expected"); }
#line 2367 "grammary.c"
    break;

  case 78:
#line 282 "grammary.y"
    { (yyval.node) = NULL; yyerror("'(' expected"); }
#line 2373 "grammary.c"
    break;

  case 79:
#line 283 "grammary.y"
    {
			(yyval.node) = poptag();
			(yyval.node)->lfor.cond = (yyvsp[-4].node);
			(yyval.node)->lfor.stmts = (yyvsp[-1].node);
			(yyval.node) = debug_add((yyval.node));
		}
#line 2384 "grammary.c"
    break;

  case 80:
#line 289 "grammary.y"
    { (yyval.node) = NULL; yyerror("'}' expected"); }
#line 2390 "grammary.c"
    break;

  case 81:
#line 290 "grammary.y"
    { (yyval.node) = NULL; yyerror("'{' expected"); }
#line 2396 "grammary.c"
    break;

  case 82:
#line 291 "grammary.y"
    { (yyval.node) = NULL; yyerror("')' expected"); }
#line 2402 "grammary.c"
    break;

  case 83:
#line 292 "grammary.y"
    { (yyval.node) = NULL; yyerror("Conditional expression expected"); }
#line 2408 "grammary.c"
    break;

  case 84:
#line 293 "grammary.y"
    { (yyval.node) = NULL; yyerror("'(' expected"); }
#line 2414 "grammary.c"
    break;

  case 85:
#line 294 "grammary.y"
    {
			(yyval.node) = poptag();
			(yyval.node)->lfor.cond = (yyvsp[-2].node);
			(yyval.node)->lfor.stmts = (yyvsp[-6].node);
			(yyval.node) = debug_add((yyval.node));
		}
#line 2425 "grammary.c"
    break;

  case 86:
#line 300 "grammary.y"
    { (yyval.node) = NULL; yyerror("';' expected"); }
#line 2431 "grammary.c"
    break;

  case 87:
#line 301 "grammary.y"
    { (yyval.node) = NULL; yyerror("')' expected"); }
#line 2437 "grammary.c"
    break;

  case 88:
#line 302 "grammary.y"
    { (yyval.node) = NULL; yyerror("Conditional expression expected"); }
#line 2443 "grammary.c"
    break;

  case 89:
#line 303 "grammary.y"
    { (yyval.node) = NULL; yyerror("'(' expected"); }
#line 2449 "grammary.c"
    break;

  case 90:
#line 304 "grammary.y"
    { (yyval.node) = NULL; yyerror("'while' expected"); }
#line 2455 "grammary.c"
    break;

  case 91:
#line 305 "grammary.y"
    { (yyval.node) = NULL; yyerror("'}' expected"); }
#line 2461 "grammary.c"
    break;

  case 92:
#line 306 "grammary.y"
    { (yyval.node) = NULL; yyerror("'{' expected"); }
#line 2467 "grammary.c"
    break;

  case 93:
#line 307 "grammary.y"
    {
			(yyval.node) = newnode(NT_FUNCTION);
			(yyval.node)->func.id = (yyvsp[-6].str);
			(yyval.node)->func.args = (yyvsp[-4].node);
			(yyval.node)->func.body = (yyvsp[-1].node);
			funcname = NULL;
			/* Adding a debug point here can never be called. */
			/* The execution of the AST will call the function's */
			/* defintion at 'optstmts', not the declaration. */
			(yyval.node) = debug_add((yyval.node));
		}
#line 2483 "grammary.c"
    break;

  case 94:
#line 318 "grammary.y"
    { (yyval.node) = NULL; yyerror("'}' expected"); funcname = NULL; }
#line 2489 "grammary.c"
    break;

  case 95:
#line 319 "grammary.y"
    { (yyval.node) = NULL; yyerror("'{' expected"); funcname = NULL; }
#line 2495 "grammary.c"
    break;

  case 96:
#line 320 "grammary.y"
    { (yyval.node) = NULL; yyerror("')' expected"); funcname = NULL; }
#line 2501 "grammary.c"
    break;

  case 97:
#line 321 "grammary.y"
    { (yyval.node) = NULL; yyerror("'(' expected"); funcname = NULL; }
#line 2507 "grammary.c"
    break;

  case 98:
#line 322 "grammary.y"
    {
			(yyval.node) = newnode(NT_IF);
			(yyval.node)->cond.cond = (yyvsp[-5].node);
			(yyval.node)->cond.ifclause = (yyvsp[-2].node);
			(yyval.node)->cond.elifclauses = gethead((yyvsp[0].node));
			(yyval.node) = debug_add((yyval.node));
		}
#line 2519 "grammary.c"
    break;

  case 99:
#line 329 "grammary.y"
    {
			(yyval.node) = newnode(NT_IF);
			(yyval.node)->cond.cond = (yyvsp[-9].node);
			(yyval.node)->cond.ifclause = (yyvsp[-6].node);
			(yyval.node)->cond.elifclauses = gethead((yyvsp[-4].node));
			(yyval.node)->cond.elseclause = (yyvsp[-1].node);
			(yyval.node) = debug_add((yyval.node));
		}
#line 2532 "grammary.c"
    break;

  case 100:
#line 337 "grammary.y"
    { (yyval.node) = NULL; yyerror("'}' expected"); }
#line 2538 "grammary.c"
    break;

  case 101:
#line 338 "grammary.y"
    { (yyval.node) = NULL; yyerror("'{' expected"); }
#line 2544 "grammary.c"
    break;

  case 102:
#line 339 "grammary.y"
    { (yyval.node) = NULL; yyerror("'}' expected"); }
#line 2550 "grammary.c"
    break;

  case 103:
#line 340 "grammary.y"
    { (yyval.node) = NULL; yyerror("'{' expected"); }
#line 2556 "grammary.c"
    break;

  case 104:
#line 341 "grammary.y"
    { (yyval.node) = NULL; yyerror("')' expected"); }
#line 2562 "grammary.c"
    break;

  case 105:
#line 342 "grammary.y"
    { (yyval.node) = NULL; yyerror("Conditional expression expected"); }
#line 2568 "grammary.c"
    break;

  case 106:
#line 343 "grammary.y"
    { (yyval.node) = NULL; yyerror("'(' expected"); }
#line 2574 "grammary.c"
    break;

  case 107:
#line 344 "grammary.y"
    { (yyval.node) = debug_add(node_new(NT_RETURN, (yyvsp[-1].node))); }
#line 2580 "grammary.c"
    break;

  case 108:
#line 345 "grammary.y"
    { (yyval.node) = debug_add(node_new(NT_RETURN, NULL)); }
#line 2586 "grammary.c"
    break;

  case 109:
#line 346 "grammary.y"
    { (yyval.node) = NULL; yyerror("Expression or ';' expected"); }
#line 2592 "grammary.c"
    break;

  case 110:
#line 347 "grammary.y"
    { (yyval.node) = NULL; yyerror("';' expected"); }
#line 2598 "grammary.c"
    break;

  case 111:
#line 348 "grammary.y"
    { (yyval.node) = debug_add(node_new(NT_BREAK, NULL)); }
#line 2604 "grammary.c"
    break;

  case 112:
#line 349 "grammary.y"
    { (yyval.node) = NULL; yyerror("';' expected"); }
#line 2610 "grammary.c"
    break;

  case 113:
#line 350 "grammary.y"
    { (yyval.node) = debug_add(node_new(NT_CONTINUE, NULL)); }
#line 2616 "grammary.c"
    break;

  case 114:
#line 351 "grammary.y"
    { (yyval.node) = NULL; yyerror("';' expected"); }
#line 2622 "grammary.c"
    break;

  case 115:
#line 354 "grammary.y"
    { (yyval.node) = NULL; }
#line 2628 "grammary.c"
    break;

  case 116:
#line 355 "grammary.y"
    { (yyval.node) = (yyvsp[0].node); if((yyvsp[-1].node)) { (yyvsp[-1].node)->next = (yyvsp[0].node); (yyvsp[0].node)->prev = (yyvsp[-1].node);} }
#line 2634 "grammary.c"
    break;

  case 117:
#line 358 "grammary.y"
    {
			(yyval.node) = newnode(NT_ELIF);
			(yyval.node)->cond.cond = (yyvsp[-4].node);
			(yyval.node)->cond.ifclause = (yyvsp[-1].node);
			(yyval.node) = debug_add((yyval.node));
		}
#line 2645 "grammary.c"
    break;

  case 118:
#line 364 "grammary.y"
    { (yyval.node) = NULL; yyerror("'}' expected"); }
#line 2651 "grammary.c"
    break;

  case 119:
#line 365 "grammary.y"
    { (yyval.node) = NULL; yyerror("'{' expected"); }
#line 2657 "grammary.c"
    break;

  case 120:
#line 366 "grammary.y"
    { (yyval.node) = NULL; yyerror("')' expected"); }
#line 2663 "grammary.c"
    break;

  case 121:
#line 367 "grammary.y"
    { (yyval.node) = NULL; yyerror("Conditional expression expected"); }
#line 2669 "grammary.c"
    break;

  case 122:
#line 368 "grammary.y"
    { (yyval.node) = NULL; yyerror("'(' expected"); }
#line 2675 "grammary.c"
    break;

  case 123:
#line 371 "grammary.y"
    { (yyval.node) = expr_new_call((yyvsp[-3].str), (yyvsp[-1].node)); }
#line 2681 "grammary.c"
    break;

  case 124:
#line 374 "grammary.y"
    { (yyval.node) = NULL; }
#line 2687 "grammary.c"
    break;

  case 125:
#line 375 "grammary.y"
    { (yyval.node) = (yyvsp[0].node); }
#line 2693 "grammary.c"
    break;

  case 126:
#line 378 "grammary.y"
    { (yyval.node) = elist_new((yyvsp[0].node)); }
#line 2699 "grammary.c"
    break;

  case 127:
#line 379 "grammary.y"
    { (yyval.node) = elist_add((yyvsp[-2].node), (yyvsp[0].node)); }
#line 2705 "grammary.c"
    break;

  case 128:
#line 380 "grammary.y"
    { (yyval.node) = (yyvsp[-2].node); yyerror("Expression expected after ','"); }
#line 2711 "grammary.c"
    break;

  case 129:
#line 381 "grammary.y"
    { (yyval.node) = (yyvsp[-1].node); yyerror("',' expected"); }
#line 2717 "grammary.c"
    break;

  case 130:
#line 384 "grammary.y"
    { (yyval.node) = (yyvsp[0].node); }
#line 2723 "grammary.c"
    break;

  case 131:
#line 385 "grammary.y"
    {
			if((yyvsp[0].node)->expr.op == OP_INT) {
				(yyval.node) = (yyvsp[0].node);
				(yyval.node)->expr.i = -(yyval.node)->expr.i;	/* Literal int negate */
			} else if((yyvsp[0].node)->expr.op == OP_FLOAT) {
				(yyval.node) = (yyvsp[0].node);
				(yyval.node)->expr.d = -(yyval.node)->expr.d;	/* Literal float negate */
			} else {
				/* Others are multiplied by -1 as in "expr * -1" */
				(yyval.node) = expr_new((yyvsp[0].node), expr_new_int(-1, UNIT_NONE), OP_MUL);
			}
		}
#line 2740 "grammary.c"
    break;

  case 132:
#line 397 "grammary.y"
    { (yyval.node) = expr_new((yyvsp[0].node), NULL, OP_NOT); }
#line 2746 "grammary.c"
    break;

  case 133:
#line 398 "grammary.y"
    { (yyval.node) = expr_new((yyvsp[0].node), NULL, OP_BNOT); }
#line 2752 "grammary.c"
    break;

  case 134:
#line 399 "grammary.y"
    { (yyval.node) = (yyvsp[-1].node); (yyval.node)->expr.inparen = 1; }
#line 2758 "grammary.c"
    break;

  case 135:
#line 400 "grammary.y"
    { (yyval.node) = expr_new((yyvsp[-2].node), (yyvsp[0].node), OP_ADD); }
#line 2764 "grammary.c"
    break;

  case 136:
#line 401 "grammary.y"
    { (yyval.node) = expr_new((yyvsp[-2].node), (yyvsp[0].node), OP_ADDOR); }
#line 2770 "grammary.c"
    break;

  case 137:
#line 402 "grammary.y"
    { (yyval.node) = expr_new((yyvsp[-2].node), (yyvsp[0].node), OP_SUB); }
#line 2776 "grammary.c"
    break;

  case 138:
#line 403 "grammary.y"
    { (yyval.node) = expr_new((yyvsp[-2].node), (yyvsp[0].node), OP_SUBOR); }
#line 2782 "grammary.c"
    break;

  case 139:
#line 404 "grammary.y"
    { (yyval.node) = expr_new((yyvsp[-2].node), (yyvsp[0].node), OP_MUL); }
#line 2788 "grammary.c"
    break;

  case 140:
#line 405 "grammary.y"
    { (yyval.node) = expr_new((yyvsp[-2].node), (yyvsp[0].node), OP_DIV); }
#line 2794 "grammary.c"
    break;

  case 141:
#line 406 "grammary.y"
    { (yyval.node) = expr_new((yyvsp[-2].node), (yyvsp[0].node), OP_MOD); }
#line 2800 "grammary.c"
    break;

  case 142:
#line 407 "grammary.y"
    { (yyval.node) = expr_new((yyvsp[-2].node), (yyvsp[0].node), OP_EQ); }
#line 2806 "grammary.c"
    break;

  case 143:
#line 408 "grammary.y"
    { (yyval.node) = expr_new((yyvsp[-2].node), (yyvsp[0].node), OP_NE); }
#line 2812 "grammary.c"
    break;

  case 144:
#line 409 "grammary.y"
    { (yyval.node) = expr_new((yyvsp[-2].node), (yyvsp[0].node), OP_GT); }
#line 2818 "grammary.c"
    break;

  case 145:
#line 410 "grammary.y"
    { (yyval.node) = expr_new((yyvsp[-2].node), (yyvsp[0].node), OP_LT); }
#line 2824 "grammary.c"
    break;

  case 146:
#line 411 "grammary.y"
    { (yyval.node) = expr_new((yyvsp[-2].node), (yyvsp[0].node), OP_GE); }
#line 2830 "grammary.c"
    break;

  case 147:
#line 412 "grammary.y"
    { (yyval.node) = expr_new((yyvsp[-2].node), (yyvsp[0].node), OP_LE); }
#line 2836 "grammary.c"
    break;

  case 148:
#line 413 "grammary.y"
    { (yyval.node) = expr_new((yyvsp[-2].node), (yyvsp[0].node), OP_LAND); }
#line 2842 "grammary.c"
    break;

  case 149:
#line 414 "grammary.y"
    { (yyval.node) = expr_new((yyvsp[-2].node), (yyvsp[0].node), OP_LOR); }
#line 2848 "grammary.c"
    break;

  case 150:
#line 415 "grammary.y"
    { (yyval.node) = expr_new((yyvsp[-2].node), (yyvsp[0].node), OP_SHL); }
#line 2854 "grammary.c"
    break;

  case 151:
#line 416 "grammary.y"
    { (yyval.node) = expr_new((yyvsp[-2].node), (yyvsp[0].node), OP_SHR); }
#line 2860 "grammary.c"
    break;

  case 152:
#line 417 "grammary.y"
    { (yyval.node) = expr_new((yyvsp[-2].node), (yyvsp[0].node), OP_BAND); }
#line 2866 "grammary.c"
    break;

  case 153:
#line 418 "grammary.y"
    { (yyval.node) = expr_new((yyvsp[-2].node), (yyvsp[0].node), OP_BOR); }
#line 2872 "grammary.c"
    break;

  case 154:
#line 419 "grammary.y"
    { (yyval.node) = expr_new((yyvsp[-2].node), (yyvsp[0].node), OP_BXOR); }
#line 2878 "grammary.c"
    break;

  case 155:
#line 420 "grammary.y"
    {(yyval.node) = expr_new_tern((yyvsp[-4].node), (yyvsp[-2].node), (yyvsp[0].node), OP_CONDEXPR); }
#line 2884 "grammary.c"
    break;

  case 156:
#line 421 "grammary.y"
    { (yyval.node) = (yyvsp[0].node); }
#line 2890 "grammary.c"
    break;

  case 157:
#line 422 "grammary.y"
    { (yyval.node) = expr_new_idx((yyvsp[-3].node), (yyvsp[-1].node)); }
#line 2896 "grammary.c"
    break;

  case 158:
#line 423 "grammary.y"
    { (yyval.node) = expr_new_idxid((yyvsp[-2].node), (yyvsp[0].str)); }
#line 2902 "grammary.c"
    break;

  case 159:
#line 424 "grammary.y"
    { (yyval.node) = expr_new_id((yyvsp[0].str)); }
#line 2908 "grammary.c"
    break;

  case 160:
#line 425 "grammary.y"
    { (yyval.node) = expr_new_int((yyvsp[-1].i), (yyvsp[0].i)); }
#line 2914 "grammary.c"
    break;

  case 161:
#line 426 "grammary.y"
    { (yyval.node) = expr_new_flt((yyvsp[-1].d), (yyvsp[0].i)); }
#line 2920 "grammary.c"
    break;

  case 162:
#line 427 "grammary.y"
    { (yyval.node) = expr_new_str((yyvsp[0].str)); }
#line 2926 "grammary.c"
    break;

  case 163:
#line 428 "grammary.y"
    { (yyval.node) = (yyvsp[0].node); }
#line 2932 "grammary.c"
    break;

  case 164:
#line 429 "grammary.y"
    { (yyval.node) = (yyvsp[0].node); }
#line 2938 "grammary.c"
    break;

  case 165:
#line 430 "grammary.y"
    { (yyval.node) = expr_new_unary((yyvsp[0].node), OP_PREINC); }
#line 2944 "grammary.c"
    break;

  case 166:
#line 431 "grammary.y"
    { (yyval.node) = expr_new_unary((yyvsp[0].node), OP_PREDEC); }
#line 2950 "grammary.c"
    break;

  case 167:
#line 432 "grammary.y"
    { (yyval.node) = expr_new_unary((yyvsp[-1].node), OP_POSTINC); check_useless((yyval.node)); }
#line 2956 "grammary.c"
    break;

  case 168:
#line 433 "grammary.y"
    { (yyval.node) = expr_new_unary((yyvsp[-1].node), OP_POSTDEC); check_useless((yyval.node)); }
#line 2962 "grammary.c"
    break;

  case 169:
#line 434 "grammary.y"
    { (yyval.node) = node_new_assign((yyvsp[-2].node), OP_ASSIGN, (yyvsp[0].node)); }
#line 2968 "grammary.c"
    break;

  case 170:
#line 435 "grammary.y"
    { (yyval.node) = node_new_assign((yyvsp[-2].node), OP_ADDASSIGN, (yyvsp[0].node)); }
#line 2974 "grammary.c"
    break;

  case 171:
#line 436 "grammary.y"
    { (yyval.node) = node_new_assign((yyvsp[-2].node), OP_ADDORASSIGN, (yyvsp[0].node)); }
#line 2980 "grammary.c"
    break;

  case 172:
#line 437 "grammary.y"
    { (yyval.node) = node_new_assign((yyvsp[-2].node), OP_SUBASSIGN, (yyvsp[0].node)); }
#line 2986 "grammary.c"
    break;

  case 173:
#line 438 "grammary.y"
    { (yyval.node) = node_new_assign((yyvsp[-2].node), OP_SUBORASSIGN, (yyvsp[0].node)); }
#line 2992 "grammary.c"
    break;

  case 174:
#line 439 "grammary.y"
    { (yyval.node) = node_new_assign((yyvsp[-2].node), OP_MULASSIGN, (yyvsp[0].node)); }
#line 2998 "grammary.c"
    break;

  case 175:
#line 440 "grammary.y"
    { (yyval.node) = node_new_assign((yyvsp[-2].node), OP_DIVASSIGN, (yyvsp[0].node)); }
#line 3004 "grammary.c"
    break;

  case 176:
#line 441 "grammary.y"
    { (yyval.node) = node_new_assign((yyvsp[-2].node), OP_MODASSIGN, (yyvsp[0].node)); }
#line 3010 "grammary.c"
    break;

  case 177:
#line 442 "grammary.y"
    { (yyval.node) = node_new_assign((yyvsp[-2].node), OP_SHLASSIGN, (yyvsp[0].node)); }
#line 3016 "grammary.c"
    break;

  case 178:
#line 443 "grammary.y"
    { (yyval.node) = node_new_assign((yyvsp[-2].node), OP_SHRASSIGN, (yyvsp[0].node)); }
#line 3022 "grammary.c"
    break;

  case 179:
#line 444 "grammary.y"
    { (yyval.node) = node_new_assign((yyvsp[-2].node), OP_BORASSIGN, (yyvsp[0].node)); }
#line 3028 "grammary.c"
    break;

  case 180:
#line 445 "grammary.y"
    { (yyval.node) = node_new_assign((yyvsp[-2].node), OP_BANDASSIGN, (yyvsp[0].node)); }
#line 3034 "grammary.c"
    break;

  case 181:
#line 446 "grammary.y"
    { (yyval.node) = node_new_assign((yyvsp[-2].node), OP_BXORASSIGN, (yyvsp[0].node)); }
#line 3040 "grammary.c"
    break;

  case 182:
#line 447 "grammary.y"
    { (yyval.node) = (yyvsp[-2].node); yyerror("Expression expected after '='"); }
#line 3046 "grammary.c"
    break;

  case 183:
#line 448 "grammary.y"
    { (yyval.node) = (yyvsp[-2].node); yyerror("Expression expected after '+='"); }
#line 3052 "grammary.c"
    break;

  case 184:
#line 449 "grammary.y"
    { (yyval.node) = (yyvsp[-2].node); yyerror("Expression expected after '+|='"); }
#line 3058 "grammary.c"
    break;

  case 185:
#line 450 "grammary.y"
    { (yyval.node) = (yyvsp[-2].node); yyerror("Expression expected after '-='"); }
#line 3064 "grammary.c"
    break;

  case 186:
#line 451 "grammary.y"
    { (yyval.node) = (yyvsp[-2].node); yyerror("Expression expected after '-|='"); }
#line 3070 "grammary.c"
    break;

  case 187:
#line 452 "grammary.y"
    { (yyval.node) = (yyvsp[-2].node); yyerror("Expression expected after '*='"); }
#line 3076 "grammary.c"
    break;

  case 188:
#line 453 "grammary.y"
    { (yyval.node) = (yyvsp[-2].node); yyerror("Expression expected after '/='"); }
#line 3082 "grammary.c"
    break;

  case 189:
#line 454 "grammary.y"
    { (yyval.node) = (yyvsp[-2].node); yyerror("Expression expected after '%%='"); }
#line 3088 "grammary.c"
    break;

  case 190:
#line 455 "grammary.y"
    { (yyval.node) = (yyvsp[-2].node); yyerror("Expression expected after '<<='"); }
#line 3094 "grammary.c"
    break;

  case 191:
#line 456 "grammary.y"
    { (yyval.node) = (yyvsp[-2].node); yyerror("Expression expected after '>>='"); }
#line 3100 "grammary.c"
    break;

  case 192:
#line 457 "grammary.y"
    { (yyval.node) = (yyvsp[-2].node); yyerror("Expression expected after '|='"); }
#line 3106 "grammary.c"
    break;

  case 193:
#line 458 "grammary.y"
    { (yyval.node) = (yyvsp[-2].node); yyerror("Expression expected after '&='"); }
#line 3112 "grammary.c"
    break;

  case 194:
#line 459 "grammary.y"
    { (yyval.node) = (yyvsp[-2].node); yyerror("Expression expected after '^='"); }
#line 3118 "grammary.c"
    break;

  case 195:
#line 462 "grammary.y"
    { (yyval.i) = UNIT_NONE; }
#line 3124 "grammary.c"
    break;

  case 196:
#line 463 "grammary.y"
    { (yyval.i) = UNIT_MM; }
#line 3130 "grammary.c"
    break;

  case 197:
#line 464 "grammary.y"
    { (yyval.i) = _UNIT_MIL; }
#line 3136 "grammary.c"
    break;

  case 198:
#line 465 "grammary.y"
    { (yyval.i) = UNIT_IN; }
#line 3142 "grammary.c"
    break;

  case 199:
#line 466 "grammary.y"
    { (yyval.i) = UNIT_DEG; }
#line 3148 "grammary.c"
    break;

  case 200:
#line 467 "grammary.y"
    { (yyval.i) = UNIT_RAD; }
#line 3154 "grammary.c"
    break;

  case 203:
#line 474 "grammary.y"
    { (yyval.node) = (yyvsp[-2].node); }
#line 3160 "grammary.c"
    break;

  case 204:
#line 475 "grammary.y"
    { (yyval.node) = vlist_new(NULL); }
#line 3166 "grammary.c"
    break;

  case 205:
#line 476 "grammary.y"
    { (yyval.node) = NULL; yyerror("'}' expected"); }
#line 3172 "grammary.c"
    break;

  case 206:
#line 477 "grammary.y"
    { (yyval.node) = (yyvsp[-2].node); yyerror("'}' expected"); }
#line 3178 "grammary.c"
    break;

  case 207:
#line 480 "grammary.y"
    { (yyval.node) = vlist_new((yyvsp[0].node)); }
#line 3184 "grammary.c"
    break;

  case 208:
#line 481 "grammary.y"
    { (yyval.node) = vlist_add((yyvsp[-2].node), (yyvsp[0].node)); }
#line 3190 "grammary.c"
    break;

  case 209:
#line 484 "grammary.y"
    { (yyval.node) = vec_new(NULL, 0); }
#line 3196 "grammary.c"
    break;

  case 210:
#line 485 "grammary.y"
    { (yyval.node) = (yyvsp[-1].node); }
#line 3202 "grammary.c"
    break;

  case 211:
#line 486 "grammary.y"
    { (yyval.node) = (yyvsp[-1].node); yyerror("']' expected"); }
#line 3208 "grammary.c"
    break;

  case 212:
#line 489 "grammary.y"
    { (yyval.node) = vec_new((yyvsp[0].node), 1); }
#line 3214 "grammary.c"
    break;

  case 213:
#line 490 "grammary.y"
    { (yyval.node) = vec_add((yyvsp[-2].node), (yyvsp[0].node)); }
#line 3220 "grammary.c"
    break;

  case 214:
#line 491 "grammary.y"
    { (yyval.node) = (yyvsp[-2].node); yyerror("Scalar expression or '-' expected"); }
#line 3226 "grammary.c"
    break;

  case 215:
#line 494 "grammary.y"
    { (yyval.node) = NULL; }
#line 3232 "grammary.c"
    break;

  case 216:
#line 495 "grammary.y"
    { (yyval.node) = (yyvsp[0].node); }
#line 3238 "grammary.c"
    break;


#line 3242 "grammary.c"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;


#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif


/*-----------------------------------------------------.
| yyreturn -- parsing is finished, return the result.  |
`-----------------------------------------------------*/
yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 498 "grammary.y"

static node_t *newnode(int type)
{
	node_t *n = calloc(1, sizeof(node_t));
	myassert(n != NULL);
	n->type = type;
	n->linenr = prevlinenr;
	n->charnr = prevcharnr;
	n->filename = filename;
	return n;
}

static node_t *newnlist(node_t *n, int i)
{
	n->nlist.na = i;
	n->nlist.nodes = calloc(i, sizeof(n->nlist.nodes[0]));
	myassert(n->nlist.nodes != NULL);
	return n;
}

static node_t *newalist(node_t *n, int i)
{
	n->alist.na = i;
	n->alist.args = calloc(i, sizeof(n->alist.args[0]));
	myassert(n->alist.args != NULL);
	return n;
}

static node_t *alist_new(wchar_t *id, int isref, node_t *e)
{
	node_t *n = newnode(NT_ARGLIST);
	myassert(!e || (e && !isref));
	newalist(n, 4);
	n->alist.n = 1;
	n->alist.args[0].id = id;
	n->alist.args[0].isref = isref;
	n->alist.args[0].expr = e;
	return n;
}

static node_t *alist_add(node_t *l, wchar_t *id, int isref, node_t *e)
{
	int i;
	int havedef = 0;
	myassert(l->type == NT_ARGLIST);
	myassert(!e || (e && !isref));
	testalloc((void **)&l->alist.args, l->alist.n, &l->alist.na, sizeof(l->alist.args[0]));
	for(i = 0; i < l->alist.n; i++) {
		if(!wcscmp(id, l->alist.args[i].id)) {
			yyerror("Argument %d's name '%ls' already used in argument %d", l->alist.n+1, id, i+1);
			return l;
		}
		if(l->alist.args[i].expr)
			havedef = 1;
	}

	if(!e && havedef)
		yyerror("Argument %d (%ls) must include default value", l->alist.n+1, id);

	if(e && isref)
		yyerror("Argument %d (%ls) cannot be both a reference and have a default value", l->alist.n+1, id);

	l->alist.args[l->alist.n].id = id;
	l->alist.args[l->alist.n].isref = isref;
	l->alist.args[l->alist.n].expr = e;
	l->alist.n++;
	return l;
}

static node_t *lvar_new(wchar_t *id, node_t *e)
{
	node_t *n = newnode(NT_LOCAL);
	n->lvar.id = id;
	n->lvar.init = e;
	return n;
}

static node_t *cvar_new(wchar_t *id, node_t *e)
{
	node_t *n = newnode(NT_CONST);
	n->cvar.id = id;
	n->cvar.init = e;
	return n;
}

static node_t *elist_new(node_t *e)
{
	node_t *n = newnode(NT_EXPRLIST);
	newnlist(n, 4);
	n->nlist.n = 1;
	n->nlist.nodes[0] = e;
	return n;
}

static node_t *elist_add(node_t *l, node_t *e)
{
	myassert(e->type == NT_EXPR);
	myassert(l->type == NT_EXPRLIST);
	testalloc((void **)&l->nlist.nodes, l->nlist.n, &l->nlist.na, sizeof(l->nlist.nodes[0]));
	l->nlist.nodes[l->nlist.n] = e;
	l->nlist.n++;
	return l;
}

static node_t *vlist_new(node_t *v)
{
	myassert(v == NULL || v->type == NT_EXPR);
	node_t *n = newnode(NT_EXPR);
	n->expr.op = OP_VECTORLIST;
	n->expr.nlist.na = 4;
	n->expr.nlist.nodes = calloc(4, sizeof(n->expr.nlist.nodes[0]));
	myassert(n->expr.nlist.nodes != NULL);
	if(v) {
		n->expr.nlist.n = 1;
		n->expr.nlist.nodes[0] = v;
	}
	return n;
}

static node_t *vlist_add(node_t *l, node_t *v)
{
	myassert(l->type == NT_EXPR);
	myassert(l->expr.op == OP_VECTORLIST);
	myassert(v->type == NT_EXPR);
	testalloc((void **)&l->expr.nlist.nodes, l->expr.nlist.n, &l->expr.nlist.na, sizeof(l->expr.nlist.nodes[0]));
	l->expr.nlist.nodes[l->expr.nlist.n] = v;
	l->expr.nlist.n++;
	return l;
}

static node_t *vec_new(node_t *e, int allownull)
{
	node_t *n = newnode(NT_EXPR);
	n->expr.op = OP_VECTOR;
	n->expr.nlist.na = 4;
	n->expr.nlist.nodes = calloc(4, sizeof(n->expr.nlist.nodes[0]));
	myassert(n->expr.nlist.nodes != NULL);
	if(allownull || e) {
		n->expr.nlist.n = 1;
		n->expr.nlist.nodes[0] = e;
	}
	return n;
}

static node_t *vec_add(node_t *v, node_t *e)
{
	myassert(v->type == NT_EXPR);
	myassert(v->expr.op == OP_VECTOR);
	testalloc((void **)&v->expr.nlist.nodes, v->expr.nlist.n, &v->expr.nlist.na, sizeof(v->expr.nlist.nodes[0]));
	v->expr.nlist.nodes[v->expr.nlist.n] = e;
	v->expr.nlist.n++;
	return v;
}

static node_t *node_new_assign(node_t *lv, int op, node_t *rv)
{
	node_t *n = newnode(NT_EXPR);
	n->expr.op = op;
	n->expr.left = lv;
	n->expr.right = rv;
	check_useless(n);
	return n;
}

static node_t *node_new(int nt, node_t *e)
{
	node_t *n = newnode(nt);
	n->eref = e;
	return n;
}

static node_t *node_add(node_t *tailnode, node_t *newnode)
{
	node_t *nnhead = newnode;
	node_t *nntail = newnode;

	/* Find real head and tail of the new node(s) */
	if(newnode) {
		while(nnhead->prev)
			nnhead = nnhead->prev;
		while(nntail->next)
			nntail = nntail->next;
	}

	if(!tailnode)
		return nntail;	/* Always return the real tail */

	/* Find the tail's real tail */
	while(tailnode->next)
		tailnode = tailnode->next;

	if(!newnode)
		return tailnode;

	/* Crosslink to append new nodes */
	tailnode->next = nnhead;
	nnhead->prev = tailnode;
	return nntail;	/* Return the real tail of the combined list */
}

static node_t *expr_new(node_t *l, node_t *r, int op)
{
	node_t *n = newnode(NT_EXPR);
	myassert(l != NULL);
	n->expr.op = op;
	n->expr.left = l;
	n->expr.right = r;
	return n;
}

static node_t *expr_new_unary(node_t *id, int op)
{
	node_t *n = newnode(NT_EXPR);
	n->expr.op = op;
	n->expr.left = id;
	return n;
}

static node_t *expr_new_tern(node_t *c, node_t *l, node_t *r, int op)
{
	node_t *n = newnode(NT_EXPR);
	myassert(c != NULL);
	myassert(l != NULL);
	myassert(r != NULL);
	n->expr.op = op;
	n->expr.cond = c;
	n->expr.left = l;
	n->expr.right = r;
	return n;
}

static node_t *expr_new_call(wchar_t *id, node_t *e)
{
	node_t *n = newnode(NT_EXPR);
	myassert(e == NULL || e->type == NT_EXPRLIST);
	n->expr.id = id;
	n->expr.args = e;
	n->expr.op = OP_CALL;
	return n;
}

static node_t *expr_new_id(wchar_t *id)
{
	node_t *n = newnode(NT_EXPR);
	n->expr.op = OP_DEREF;
	n->expr.id = id;
	if(!wcscmp(L"__global_offset", n->expr.id) || !wcscmp(L"__global_position", n->expr.id))
		rtwarning(n, "Using internal variable '%ls' strongly discouraged", n->expr.id);
	return n;
}

static node_t *expr_new_idx(node_t *d, node_t *e)
{
	node_t *n = newnode(NT_EXPR);
	n->expr.op = OP_INDEX;
	n->expr.left = d;
	n->expr.right = e;
	return n;
}

static const wchar_t axisnames[] = L"xyzabcuvw";

static node_t *expr_new_idxid(node_t *d, wchar_t *id)
{
	node_t *n = newnode(NT_EXPR);
	assert(id != NULL);
	n->expr.op = OP_INDEXID;
	n->expr.left = d;
	wchar_t *cptr = wcschr(axisnames, id[0]);
	if(!cptr || 1 != wcslen(id))
		yyerror("Index can only be an axis name");
	n->expr.right = expr_new_int(cptr - axisnames, UNIT_NONE);
	free(id);
	return n;
}

static node_t *expr_new_int(int i, int unit)
{
	node_t *n = newnode(NT_EXPR);
	if(unit == _UNIT_MIL) {
		n->expr.op = OP_FLOAT;
		n->expr.d = (double)i / 1000.0;
		n->expr.unit = UNIT_IN;
	} else {
		n->expr.op = OP_INT;
		n->expr.i = i;
		n->expr.unit = unit;
	}
	return n;
}

static node_t *expr_new_flt(double d, int unit)
{
	node_t *n = newnode(NT_EXPR);
	n->expr.op = OP_FLOAT;
	if(unit == _UNIT_MIL) {
		d /= 1000.0;
		unit = UNIT_IN;
	}
	n->expr.d = d;
	n->expr.unit = unit;
	return n;
}

static node_t *expr_new_str(wchar_t *str)
{
	node_t *n = newnode(NT_EXPR);
	n->expr.op = OP_STRING;
	n->expr.str.chs = str;
	n->expr.str.n = wcslen(str);
	n->expr.str.na = n->expr.str.n + 1;
	return n;
}

static node_t **nodestack;
static int nnodestack;
static int nanodestack;

static node_t *pushtag(node_t *n)
{
	if(!nodestack) {
		nodestack = calloc(16, sizeof(*nodestack));
		assert(nodestack != NULL);
		nnodestack = 0;
		nanodestack = 16;
	} else if(nnodestack >= nanodestack) {
		nodestack = realloc(nodestack, nanodestack * 2 * sizeof(*nodestack));
		assert(nodestack != NULL);
		nanodestack *= 2;
	}
	nodestack[nnodestack] = n;
	nnodestack++;
	return n;
}

static node_t *poptag(void)
{
	assert(nnodestack > 0);
	nnodestack--;
	return nodestack[nnodestack];
}

static node_t *gethead(node_t *n)
{
	if(!n)
		return NULL;
	while(n->prev)
		n = n->prev;
	return n;
}

void node_delete(node_t *head)
{
	node_t *n, *next;
	int i;

	assert(nnodestack == 0);

	if(nodestack) {
		free(nodestack);
		nodestack = NULL;
	}

	if(!head)
		return;

	for(n = head; n; n = next) {
		next = n->next;
		switch(n->type) {
		case NT_EXPRLIST:
			for(i = 0; i < n->nlist.n; i++)
				node_delete(n->nlist.nodes[i]);
			free(n->nlist.nodes);
			break;
		case NT_ARGLIST:
			for(i = 0; i < n->alist.n; i++) {
				node_delete(n->alist.args[i].expr);
				free(n->alist.args[i].id);
			}
			free(n->alist.args);
			break;
		case NT_RETURN:
			node_delete(n->eref);
			break;
		case NT_LOCAL:
			node_delete(n->lvar.init);
			free(n->lvar.id);
			break;
		case NT_CONST:
			node_delete(n->cvar.init);
			free(n->cvar.id);
			break;
		case NT_BREAK:
		case NT_CONTINUE:
		case NT_INVALID:
		case NT_DEBUG:
			break;
		case NT_EXPR:
			node_delete(n->expr.left);
			node_delete(n->expr.right);
			switch(n->expr.op) {
			case OP_NULL:
				break;
			case OP_ADD:
			case OP_ADDOR:
			case OP_SUB:
			case OP_SUBOR:
			case OP_MUL:
			case OP_DIV:
			case OP_MOD:
			case OP_LOR:
			case OP_LAND:
			case OP_BOR:
			case OP_BXOR:
			case OP_BAND:
			case OP_BNOT:
			case OP_EQ:
			case OP_NE:
			case OP_GT:
			case OP_LT:
			case OP_GE:
			case OP_LE:
			case OP_SHL:
			case OP_SHR:
			case OP_NOT:
			case OP_INT:
			case OP_FLOAT:
				break;
			case OP_CONDEXPR:
				node_delete(n->expr.cond);
				break;
			case OP_CALL:
				node_delete(n->expr.args);
				free(n->expr.id);
				break;
			case OP_ASSIGN:
			case OP_ADDASSIGN:
			case OP_ADDORASSIGN:
			case OP_SUBASSIGN:
			case OP_SUBORASSIGN:
			case OP_MULASSIGN:
			case OP_DIVASSIGN:
			case OP_MODASSIGN:
			case OP_SHLASSIGN:
			case OP_SHRASSIGN:
			case OP_BORASSIGN:
			case OP_BANDASSIGN:
			case OP_BXORASSIGN:
			case OP_INDEX:
			case OP_INDEXID:
				break;
			case OP_STRING:
				free(n->expr.str.chs);
				break;
			case OP_DEREF:
				free(n->expr.id);
				break;
			case OP_PREINC:
			case OP_PREDEC:
			case OP_POSTINC:
			case OP_POSTDEC:
				break;
			case OP_VECTOR:
			case OP_VECTORLIST:
				for(i = 0; i < n->expr.nlist.n; i++)
					node_delete(n->expr.nlist.nodes[i]);
				free(n->expr.nlist.nodes);
				break;
			}
			break;
		case NT_IF:
			node_delete(n->cond.cond);
			node_delete(n->cond.ifclause);
			node_delete(n->cond.elifclauses);
			node_delete(n->cond.elseclause);
			break;
		case NT_ELIF:
			node_delete(n->cond.cond);
			node_delete(n->cond.ifclause);
			break;
		case NT_FOR:
		case NT_WHILE:
		case NT_DOWHILE:
			node_delete(n->lfor.stmts);
			node_delete(n->lfor.init);
			node_delete(n->lfor.cond);
			node_delete(n->lfor.inc);
			break;
		case NT_FOREACH:
		case NT_REPEAT:
			node_delete(n->lfe.stmts);
			node_delete(n->lfe.src);
			if(n->lfe.dst)
				free(n->lfe.dst);
			break;
		case NT_FUNCTION:
			node_delete(n->func.args);
			node_delete(n->func.body);
			free(n->func.id);
			break;
		}
		free(n);
	}
}

static const wchar_t **funcs;
static int nfuncs;
static int nafuncs;

const builtins_t *find_builtin(const wchar_t *s);

static void checkfuncname(const wchar_t *fn)
{
	int i;
	if(find_builtin(fn)) {
		yyerror("Function name '%ls' reserved as built-in function", fn);
		return;
	}
	for(i = 0; i < nfuncs; i++) {
		if(!wcscmp(fn , funcs[i])) {
			yyerror("Function name '%ls' is already defined", fn);
			return;
		}
	}
	testalloc((void **)&funcs, nfuncs, &nafuncs, sizeof(*funcs));
	funcs[nfuncs] = fn;
	nfuncs++;
}

void parser_cleanup(void)
{
	if(funcs) {
		free(funcs);
		funcs = NULL;
		nafuncs = nfuncs = 0;
	}
}

static int findderef(const node_t *n)
{
	const node_t *lv;
	/* Check if lvalue (left) is dereference-able */
	assert(n->expr.left != NULL);
	if(n->expr.inparen)
		return -1;
	for(lv = n->expr.left; lv; lv = lv->expr.left) {
		myassert(n->type == NT_EXPR);
		if(lv->expr.inparen)
			return -1;
		if(!lv->expr.left)
			break;
	}
	assert(lv != NULL);
	if(lv->expr.op == OP_DEREF) {
		if(!wcscmp(L"__global_offset", lv->expr.id) || !wcscmp(L"__global_position", lv->expr.id))
			yyerror("Assigning to read-only variable '%ls' not allowed", lv->expr.id);
		return 1;		/* Deref operation works on IDENT --> OK */
	}
	return -1;
}

static void check_useless(const node_t *n)
{
	assert(n != NULL);
	myassert(n->type == NT_EXPR);

	switch(n->expr.op) {
	case OP_CALL:
		/* Calls are fine */
		return;
	case OP_POSTINC:
	case OP_POSTDEC:
		/* Post inc/dec on a constant has no effect */
		if(findderef(n) >= 0)
			return;
		yyerror("Statement post-%s on a constant has no effect", n->expr.op == OP_POSTINC ? "increment" : "decrement");
		break;
	case OP_ASSIGN:
	case OP_ADDASSIGN:
	case OP_ADDORASSIGN:
	case OP_SUBASSIGN:
	case OP_SUBORASSIGN:
	case OP_MULASSIGN:
	case OP_DIVASSIGN:
	case OP_MODASSIGN:
	case OP_SHLASSIGN:
	case OP_SHRASSIGN:
	case OP_BORASSIGN:
	case OP_BANDASSIGN:
	case OP_BXORASSIGN:
		if(findderef(n) >= 0 && !n->expr.inparen)
			return;
		/* Fallthrough */
	default:
		yyerror("Lvalue not a variable, cannot be dereferenced");
		return;
	}
}

static void check_const_expr(const node_t *n)
{
	if(!n)
		return;

	myassert(n->type == NT_EXPR);

	switch(n->expr.op) {
	case OP_CALL:
		/* Calls are fine */
		return;
	case OP_ASSIGN:
	case OP_ADDASSIGN:
	case OP_ADDORASSIGN:
	case OP_SUBASSIGN:
	case OP_SUBORASSIGN:
	case OP_MULASSIGN:
	case OP_DIVASSIGN:
	case OP_MODASSIGN:
	case OP_SHLASSIGN:
	case OP_SHRASSIGN:
	case OP_BORASSIGN:
	case OP_BANDASSIGN:
	case OP_BXORASSIGN:
		/* We already check assignments */
		return;
	case OP_PREINC:
	case OP_PREDEC:
	case OP_POSTINC:
	case OP_POSTDEC:
		if(findderef(n) >= 0)
			return;
		/* Fallthrough */
	default:
		yyerror("Statement has no effect");
		return;
	}
}

static void check_boolean_expr(const node_t *n)
{
	assert(n != NULL);
	myassert(n->type == NT_EXPR);

	switch(n->expr.op) {
	case OP_CALL:
		return;
	case OP_ASSIGN:
	case OP_ADDASSIGN:
	case OP_ADDORASSIGN:
	case OP_SUBASSIGN:
	case OP_SUBORASSIGN:
	case OP_MULASSIGN:
	case OP_DIVASSIGN:
	case OP_MODASSIGN:
	case OP_SHLASSIGN:
	case OP_SHRASSIGN:
	case OP_BORASSIGN:
	case OP_BANDASSIGN:
	case OP_BXORASSIGN:
		if(!n->expr.inparen) {
			rtwarning(n, "Assignment in boolean expression may be an inadvertent error, use () to force");
			return;
		}
	default:
		if(n->expr.left)
			check_boolean_expr(n->expr.left);
		if(n->expr.right)
			check_boolean_expr(n->expr.right);
	}
}

/* Create a list of nodes for the current file in parsed linenr order */
static void dbgnode_add(node_t *n)
{
	dbgfile_t *df;
	df = &dbgfiles[dbgfileidx];
	testalloc2((void **)&df->nodes, df->nnodes, &df->nanodes, sizeof(df->nodes[0]), 64);
	df->nodes[df->nnodes] = n;
	df->nnodes++;
}

static int is_magic_filename(const char *fn)
{
	return !strcmp(fn, "<cmd-line defines>") || !strcmp(fn, "<gcmc-internal constants>");
}

static node_t *debug_add(node_t *n)
{
	/* Skip debug nodes on all internally added nodes */
	if(cl_debug && !is_magic_filename(n->filename)) {
		node_t *dn = newnode(NT_DEBUG);
		n = node_add(dn, n);
		dbgnode_add(dn);
	}
	return n;
}

/* This creates the first dbgfile object */
void dbgfile_init(void)
{
	const char *cptr;
	if(!cl_debug)
		return;
	testalloc((void **)&dbgfiles, ndbgfiles, &nadbgfiles, sizeof(dbgfiles[0]));
	cptr = strrchr(filename, '/');
	if(!cptr)
		cptr = filename;
	else
		cptr++;
	dbgfiles[ndbgfiles].filename = filename;
	dbgfiles[ndbgfiles].shortname = cptr;
	ndbgfiles++;
	dbgfileidx = 0;
}

/* Called from the lexer handling includes */
void dbgfile_push(void)
{
	const char *cptr;
	if(!cl_debug || is_magic_filename(filename))
		return;
	/* Push the current dbgfile reference onto the stack */
	testalloc((void **)&dbgstack, ndbgstack, &nadbgstack, sizeof(dbgstack[0]));
	dbgstack[ndbgstack++] = dbgfileidx;

	/* Create a new instance of dbgfile */
	testalloc((void **)&dbgfiles, ndbgfiles, &nadbgfiles, sizeof(dbgfiles[0]));
	cptr = strrchr(filename, '/');
	if(!cptr)
		cptr = filename;
	else
		cptr++;
	dbgfiles[ndbgfiles].filename = filename;
	dbgfiles[ndbgfiles].shortname = cptr;
	dbgfileidx = ndbgfiles++;
}

void dbgfile_pop(void)
{
	if(!cl_debug || is_magic_filename(filename))
		return;
	if(ndbgstack)
		dbgfileidx = dbgstack[--ndbgstack];
	else
		yyfatal("Underflow of includes in debug records");
}

void dbgfile_add(FILE *fp)
{
	/* The file is EOF */
	/* linenr should be indicating the number of '\n' read + 1 */
	long fl, fr;
	int ln;
	char *cptr;
	clearerr(fp);
	fseek(fp, 0, SEEK_END);
	fl = ftell(fp);

	/* Read the entire file into memory */
	dbgfiles[dbgfileidx].filelines = cptr = (char *)calloc(fl + 1, 1);
	assert(dbgfiles[dbgfileidx].filelines != NULL);
	fseek(fp, 0, SEEK_SET);
	fr = (long)fread((void *)dbgfiles[dbgfileidx].filelines, 1, fl, fp);
	assert(fl == fr);

	/* Get memory for the line pointers */
	dbgfiles[dbgfileidx].lines = (const char **)calloc(linenr, sizeof(dbgfiles[dbgfileidx].lines[0]));
	assert(dbgfiles[dbgfileidx].lines != NULL);
	dbgfiles[dbgfileidx].nlines = linenr - 1;

	/* Tag the beginning of each line in the lines array */
	for(fr = 0, ln = 0; fr < fl; ) {
		dbgfiles[dbgfileidx].lines[ln++] = cptr;
		while(fr < fl) {
			if(*cptr == '\r') {
				*cptr++ = '\0';
				fr++;
			} else if(*cptr == '\n') {
				*cptr++ = '\0';
				fr++;
				break;
			}
			cptr++;
			fr++;
		}
	}
}
