#!/bin/bash


function buildgcmczip() {
	mingw${1}-configure

	VERSION="$(grep -w PACKAGE_VERSION config.h | cut -d' ' -f3 | sed -e's/"//g')"

	D="gcmc${1}-${VERSION}"
	rm -rf "${D}" "${D}.zip"
	mkdir "${D}" "${D}/library" "${D}/doc" "${D}/example" "${D}/contrib"

	mingw${1}-make clean all || exit

	cp src/gcmc.exe "${D}/"
	mingw-strip "${D}/gcmc.exe"
	copydeps --verbose src/gcmc.exe "${D}/" || exit
	for i in "${D}/*.dll"; do
		mingw-strip "$i"
	done

	# Copy the library routines
	cp	library/canned_drill.inc.gcmc \
		library/engrave.inc.gcmc \
		library/tracepath.inc.gcmc \
		library/tracepath_comp.inc.gcmc \
		library/varcs.inc.gcmc \
		library/vbezier.inc.gcmc \
		"${D}/library"

	# Copy the documentation
	man2html -r doc/gcmc.1 | tail -n+3 | sed -e's/<BODY>/<BODY id="gcmc_body">/' > "${D}/doc/gcmc.1.html"
	cp	doc/gcmc-funcref.html \
		doc/gcmc-intro.html \
		doc/gcmc-syntax.html \
		doc/gcmc-library.html \
		doc/gcmc.css \
		doc/involute-basics.png \
		doc/involute-tooth-side.png \
		gcmcfont/fontdata/font_hsans_1.svg \
		gcmcfont/fontdata/font_hsans_1_rs.svg \
		gcmcfont/fontdata/font_hsans_2.svg \
		gcmcfont/fontdata/font_hscript_1.svg \
		gcmcfont/fontdata/font_hscript_2.svg \
		gcmcfont/fontdata/font_htimes.svg \
		gcmcfont/fontdata/font_htimes_bold.svg \
		gcmcfont/fontdata/font_htimes_italic.svg \
		gcmcfont/fontdata/font_htimes_italic_bold.svg \
		TODO \
		README \
		COPYING \
		"${D}/doc"

	# Copy the examples
	cp	example/ball-in-cube.gcmc \
		example/bezier.gcmc \
		example/canned.gcmc \
		example/cc_hole.gcmc \
		example/cc_hole.inc.gcmc \
		example/colors.gcmc \
		example/cutter.gcmc \
		example/cycloids.gcmc \
		example/edm-pro-box.gcmc \
		example/floret-vogel.gcmc \
		example/involute-gear.gcmc \
		example/involute-gear.inc.gcmc \
		example/text-fonts.gcmc \
		example/text-on-circle.gcmc \
		example/tool-compensate.gcmc \
		example/trochoidal.gcmc \
		example/varcs.gcmc \
		example/wheels.gcmc \
		example/examples.txt \
		example/buildexample.sh \
		"${D}/example"

	# Copy the contributed stuff
	cp	contrib/gcmc.vim \
		"${D}/contrib"

	zip -r "${D}.zip" "${D}/"
	rm -rf "${D}"
}

buildgcmczip 32
buildgcmczip 64
